
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?php echo __APPNAME__; ?> - Login</title>

        <!-- Custom fonts for this template-->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="/_styles/sbadmin2/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <!-- Custom styles for this template-->
        <link href="/_styles/sbadmin2/css/sb-admin-2.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
       
    </head>
    <body class="bg-gray-600">

        <div class="container">
            <!-- Outer Row -->
            <div class="row justify-content-center">

                <div class="col-xl-10 col-lg-12 col-md-9">
                    <div class="card o-hidden border-0 shadow-lg my-5">
                        <div class="card-body p-0">
                            <!-- Nested Row within Card Body -->
                            <div class="row"> 

                                <div class="col-lg-6 d-none d-lg-block bg-qaccess-image"></div>
                                <div class="col-lg-6">
                                    <div class="p-5">
                                        <div class="text-center">
                                            <div class="sidebar-brand-icon">
                                                <h2 class="text-black1">
                                                    <a href="<?php echo __URL__;?>">
                                                        <?php echo __APPNAME__; ?>
                                                    </a>
                                                </h2>
                                            </div>
                                            <p class="text-gray-500">please introduce yourself...</p>
                                        </div>
                                        <form action="/welcome/login/go" class="user" method='post'>
                                            <div class="form-group">
                                                <input type="text" class="form-control form-control-user"
                                                       id="f_username" name='f_username' 
                                                       aria-describedby="emailHelp" autofocus="" required  placeholder="Enter user...">
                                            </div>
                                            <div class="form-group">
                                                <input type="password" class="form-control form-control-user" 
                                                       id="f_password" name='f_password' required placeholder="Password">
                                            </div>
                                            <button type='submit' class="btn btn-primary btn-user btn-block" >
                                                Login
                                            </button>
                                            <hr>
                                            <a href="/qaccess/account/recovery" class="btn btn-gray btn-user btn-block" disabled='true'>
                                                <i class="fab fa-google fa-fw"></i> Forgot Password?
                                            </a>

                                        </form>
                                        <hr>
                                        <div class="text-center">
                                            <div class="alert alert-primary">
                                                For security reasons your IP address ( <strong><?php echo filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_SANITIZE_SPECIAL_CHARS); ?></strong>) 
                                                as been stored.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
