<div class="container-fluid">

    <section class="content-header">
        <h5 class="text-danger">
            <i class="fa fa-folder fa-2x"></i> System profiles
        </h5>
    </section> 


    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold">
                total:<span class="badge" id="total"><?php echo count($Profiles);?></span>
            </h6>
            <div class="dropdown no-arrow">
                <a href="#" class="dropdown-item"
                       data-target="#modalLg" 
                       data-remote="/qaccess/profile/add"
                       data-toggle="modal"
                       data-titlemodal='Add Profile'
                       >
                        <span class="icon">
                            <i class="fas fa-plus-square"></i>
                        </span>
                        <span class="text">Add Profile</span>
                    </a>
            </div>
        </div>
        <!-- Card Body -->
        <div class="card-body">
            <section id='results' class='table-responsive'>
                <div id="loader"></div>
                <table class="table_search table table-bordered table-condensed table-hover">
                    <thead class='bg-gray-600 text-white'>
                        <tr class="small">
                            <td class="bg-gray-800"></td>
                            <td>#</td>
                            <td>Profile Name</td>
                            <td>Namespace Landing</td>
                            <td>Layout</td>

                        </tr>
                    </thead>
                    <tbody class="small" id="list_Profile">
                        <?php foreach($Profiles AS $Profile):?>
                        <tr>
                            <td>
                                
                            </td>
                            <td><?php echo $Profile->id_profile;?></td>
                            <td><?php echo $Profile->profile_name;?></td>
                            <td><?php echo $Profile->namespace_landing;?></td>
                            <td><?php echo $Profile->layout;?></td>
                        </tr>
                        
                        <?php endforeach;?>
                    </tbody>
                </table>
            </section>
        </div>
    </div>

</div>