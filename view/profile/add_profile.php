<div class="modal-body">
    <div class="card shadow mb-4">
        <div class="card-body">
            <form action="/qaccess/profile/save" 
                  id="formQerana" name="formQerana" method="POST" class="form-horizontal"
                  accept-charset="utf-8">

                <?php echo $kerana_token; ?>
                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Grabar</button> &nbsp;
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancelar
                    </button>
                </header>
                <div class='form-group form-group-sm row small'> 
                    <label for='f_profile_name' class='col-sm-3 col-form-label'>Profile Name</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_profile_name' name='f_profile_name' 

                                   class='form-control form-control-sm' required   />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_namespace_landing' class='col-sm-3 col-form-label'>Namespace Landing</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_namespace_landing' name='f_namespace_landing' 

                                   class='form-control form-control-sm' required   />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_layout' class='col-sm-3 col-form-label'>Layout</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_layout' name='f_layout' 

                                   class='form-control form-control-sm' required   />
                        </div>   
                    </div>   
                </div>   

                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Grabar</button> &nbsp;
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancelar
                    </button>
                </header>

            </form>
        </div>
    </div>
</div>
<script>

// submit form
    $('#formQerana').submit(function (e)
    {
        e.preventDefault();
        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function (data)
            {
                $('#modalLg').modal('hide');
                location.reload();
            }
        });


    });


</script>


