<style>
    #register label{
        margin-right:5px;
    }
    #register input {
        padding: 5px 14px;
        border: 1px solid #d5d9da;
        box-shadow: 0 0 9px #0E34F5 inset;
        width: 272px;
        font-size: 1em;
        height: 25px;
    }
    #register .short{
        font-weight:bold;
        color:#FF0000;
        font-size:larger;
    }
    #register .weak{
        font-weight:bold;
        color:orange;
        font-size:larger;
    }
    #register .good{
        font-weight:bold;
        color:#2D98F3;
        font-size:larger;
    }
    #register .strong{
        font-weight:bold;
        color: limegreen;
        font-size:larger;
    }
    Copy


</style>

<div class="modal-body">
    <div class="card shadow mb-4">
        <div class="card-body">
            <div id="response"></div>

            <form action="/welcome/welcome/dochangemypassword" 
                  id="formChangePassword" name="formChangePassword" method="POST" class="form-horizontal"
                  accept-charset="utf-8">
                <input type="hidden" name="f_id_user" id="f_id_user" value="<?php echo $User->id_user; ?>">
                <?php echo $kerana_token; ?>
                <header class="breadcrumb">

                    <button type="submit" class="btn btn-primary btn-sm">Cambiar</button> &nbsp;
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancelar
                    </button>
                </header>
                <div class='form-group form-group-sm row small'> 
                    <label for='s_current_password' class='col-sm-3 col-form-label'>Contrase&ntilde;a actual </label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='password' id='s_current_password' name='s_current_password' 

                                   class='form-control form-control-sm' required   value='' />
                        </div>   
                    </div>   
                </div>   
                <div class="card shadow mb-4">
                    <div class="alert alert-info" role="alert">
                        La contrase&ntilde;a debe cumplir con los siguientes requisitos
                    </div>
                    <ul class="small">
                        <li class="text-dark" id="min8">M&iacute;nimo 8 caracteres</li>
                        <li class="text-dark">M&iacute;nimo 1 n&uacute;mero</li>
                        <li class="text-dark">M&iacute;nimo 1 letra en may&uacute;sculas</li>
                        <li class="text-dark">M&iacute;nimo 1 letra en min&uacute;sculas</li>
                    </ul>
                    <div class="card-body">
                        <div class="progress">
                            <div id="result" class="progress-bar progress-bar-striped text-gray-900" 
                                 role="progressbar" style="width: 0%" 
                                 aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for='f_password' class='col-sm-4 control-label'>Nueva contrase&ntilde;a
                                <span class="input-group-addon"><i onclick="tooglePassword()" class="fa fa-eye"></i></span>
                            </label>
                            <div class="col col-sm-8">
                                <input type="password" id="f_password" name="f_password" 
                                       class="form-control form-control-user"  
                                       pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                                       required 
                                       title="Debe tener almenos un numero, una letra en MAY y otra en MIN, y por lo
                                       menos 8 caracteres"/>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for='f_password_verification' class='col-sm-4 control-label'>Repite contrase&ntilde;a
                                <span class="input-group-addon"><i onclick="tooglePassword()" class="fa fa-eye"></i></span>
                            </label> 
                            <div class="col col-sm-8">
                                <input type="password" id="f_password_verification" name="f_password_verification" 
                                       class="form-control form-control-user"  
                                       pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                                       required 
                                       title="Debe tener almenos un numero, una letra en MAY y otra en MIN, y por lo
                                       menos 8 caracteres"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div id="div_error_password" clasS="alert alert-danger d-none"></div>
                        </div>
                    </div>
                </div>



            </form>
        </div>
    </div>
</div>
<script>

    $(document).ready(function () {
        $('#f_password').keyup(function () {
            $('#result').html(checkStrength($('#f_password').val()));
        });
        function checkStrength(password) {
            var strength = 0;
            if (password.length < 6) {
                $('#result').removeClass();
                $('#result').addClass('progress-bar progress-bar-striped bg-warning');
                $("#result").css({"width": "10%"});
                return 'Muy corto';
            }
            if (password.length > 7)
                strength += 1;
// If password contains both lower and uppercase characters, increase strength value.
            if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
                strength += 1;
// If it has numbers and characters, increase strength value.
            if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
                strength += 1;
// If it has one special character, increase strength value.
            if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
                strength += 1;
// If it has two special characters, increase strength value.
            if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
                strength += 1;
// Calculated strength value, we can return messages
// If value is less than 2
            if (strength < 2) {
                $('#result').removeClass();
                $('#result').addClass('progress-bar progress-bar-striped bg-danger');
                $("#result").css({"width": "15%"});
                return 'Débil';
            } else if (strength === 2) {
                $('#result').removeClass();
                $('#result').addClass('progress-bar progress-bar-striped bg-info');
                $("#result").css({"width": "70%"});
                return 'Casi pero no';
            } else {
                $('#result').removeClass();
                $('#result').addClass('progress-bar progress-bar-striped bg-success');
                $("#result").css({"width": "95%"});
                return 'Fuerte';
            }
        }
    });


    /**
     * -------------------------------------------------------------------------
     * Toogle password type
     * -------------------------------------------------------------------------
     * @returns {undefined}
     */
    function tooglePassword() {

        if ($('#f_password').prop('type') === 'password') {
            $('#f_password').prop('type', 'text');
        } else {
            $('#f_password').prop('type', 'password');
        }
        if ($('#f_password_verification').prop('type') === 'password') {
            $('#f_password_verification').prop('type', 'text');
        } else {
            $('#f_password_verification').prop('type', 'password');
        }


    }



//// submit form
    $('#formChangePassword').submit(function (e)
    {
        e.preventDefault();
        var form = $(this);
        var url = form.attr('action');

        var pass0 = $('#f_password').val();
        var pass1 = $('#f_password_verification').val();

        if (pass0 !== pass1) {
            $('#div_error_password').removeClass('d-none');
            $('#div_error_password').html('La contraseñas no coinciden');
            return false;
        }


        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function (data)
            {
                $('#formChangePassword').hide();
                $('#response').html(data);

            }
        });


    });


</script>


