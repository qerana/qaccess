<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo __APPNAME__; ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="/_styles/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="/_styles/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="/_styles/adminlte/bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="/_styles/adminlte/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="/_styles/adminlte/dist/css/skins/skin-green-light.min.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="/_styles/adminlte/bower_components/morris.js/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="/_styles/adminlte/bower_components/jvectormap/jquery-jvectormap.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="/_styles/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="/_styles/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="/_styles/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <script>


            $("#formChangePassword").submit(function () {

                var pass0 = $('#f_password').val();
                var pass1 = $('#f_password1').val();
                if (pass0 !== pass1) {
                    $('#div_error_password').removeClass('hidden');
                    $('#div_error_password').html('Las contraseñas no coinciden');
                    return false;
                } else {
                    return true;
                }

            });


            /**
             * -------------------------------------------------------------------------
             * Toogle password type
             * -------------------------------------------------------------------------
             * @returns {undefined}
             */
            function tooglePassword() {

                if ($('#f_password').prop('type') === 'password') {
                    $('#f_password').prop('type', 'text');
                } else {
                    $('#f_password').prop('type', 'password');
                }
                if ($('#f_password1').prop('type') === 'password') {
                    $('#f_password1').prop('type', 'text');
                } else {
                    $('#f_password1').prop('type', 'password');
                }


            }

        </script>
    </head>

    <body class="hold-transition skin-green-light login-page">
        <div class="login-box">

            <!-- /.login-logo -->
            <div class="login-box-body">
                <div class="login-logo small">
                    <a  href="<?php echo __URL__; ?>" class="small">
                        <b class="text-black small"> <i class="fa fa-gears fa-2x"></i>
                            ges<span class="text-blue small">CAE</span></b>
                        <br>
                        <span class="text-warning small">Recuperaci&oacute;n de cuenta</span>
                    </a>
                </div>

                <form action="<?php echo __URL__; ?>/access/recovery/doActivateAccount" 
                      id="formChangePassword" name="formChangePassword" method="POST" class="form-horizontal"
                      accept-charset="utf-8">
                    

                    <header class="breadcrumb">
                        <span class="help-block" class="text-primary" id="estado_email">
                            Usa contrase&ntilde;as fuertes, por lo menos una mayuscula, un numero.<br>
                            <strong>Y muy importante, no uses la misma contrase&ntilde;a para todos los sitios</strong><br>
                            Si se hackea uno de los sitios ya pueden obtener tu contrase&ntilde;a de acceso a los dem&aacute;s sitios.
                        </span>
                       
                        <button type="submit"  id="btn-save-new-user btn-flat" class="btn btn-success">
                            Recuperar cuenta
                        </button>
                    </header>
                    <div class="alert alert-danger hidden" id="div_error_password"></div>


                    <div class='data-user form-group form-group-sm'> 
                        <label for='f_name' class='col-sm-2 control-label'>Password</label> 
                        <div class='col-sm-12'> 
                            <div class='input-group col-sm-8'> 
                                <input type="password" id="f_password" name="f_password" 
                                       class="form-control"  
                                       pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                                       required 
                                       title="Debe tener almenos un numero, una letra en MAY y otra en MIN, y por lo
                                       menos 8 caracteres"/>
                                <span class="input-group-addon"><i onclick="tooglePassword()" class="fa fa-eye"></i></span>
                            </div> 
                        </div> 
                    </div> 
                    <div class='data-user form-group form-group-sm'> 
                        <label for='f_name' class='col-sm-2 control-label'>Repite password</label> 
                        <div class='col-sm-12'> 
                            <div class='input-group col-sm-8'> 
                                <input type="password" id="f_password1" name="f_password1" 
                                       class="form-control"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" 
                                       title="Debe tener almenos un numero, una letra en MAY y otra en MIN, y por lo
                                       menos 8 caracteres"
                                       required   />
                                <span class="input-group-addon"><i class="fa fa-eye"></i></span>
                            </div> 
                        </div> 
                    </div> 
                </form>

                <!-- /.social-auth-links -->

            </div>
            <div class="alert alert-warning">
                Tu dirección ip ( <strong><?php echo filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_SANITIZE_SPECIAL_CHARS); ?></strong>) 
                quedará registrada por motivos de seguridad.
            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->

        <!-- jQuery 3 -->
        <script src="/_styles/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="/_styles/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- iCheck -->
        <script src="/_styles/adminlte/plugins/iCheck/icheck.min.js"></script>
        <script>
                                    $(function () {
                                        $('input').iCheck({
                                            checkboxClass: 'icheckbox_square-blue',
                                            radioClass: 'iradio_square-blue',
                                            increaseArea: '20%' // optional
                                        });
                                    });
        </script>
    </body>
</html>
