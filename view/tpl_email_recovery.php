<html>
<head>
<style type="text/css">
.emailBody {
                font-family: Arial, Helvetica, sans-serif;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                
           
            }

            .emailBody .emailText {
                width: 680px;
                position: relative;
                margin: 5% auto;
                padding: 5px 20px 13px 20px;
                border-radius: 10px;
                background: #fff;
                background: -moz-linear-gradient(#fff, #999);
                background: -webkit-linear-gradient(#fff, #999);
                background: -o-linear-gradient(#fff, #999);
            }

</style>
<body>
<div class="emailBody">
    <div class="emailText">
         <h3 style="margin-bottom:1px">
            <span style="font-weight:normal">hola </span> [{nombre_usuario}]</h3>
        <hr style="border-bottm:1px solid $ccc">
        <p style="padding:10px;margin:10px 3px 3px 3px;background-color:#fff; border-radius: 10px;">
        Has solicitidado la reactivaci&oacute;n de tu cuenta de [{app}]<br>
        Recuerda que tu usuario es <b>[{usuario}]</b><br>
        <b>Click [{url}] para reactivar tu cuenta.</b>
        </p>
        <p style="font-size:12px"><i>Si tienes problemas al dar click, copia y pega esta url en tu navegador.<br>
        [{url_string}]
        </i></p>
        <p style="font-size:13px; color:#000">
            EL link de reactivac&oacute;n solo es v&aacute;lido para un uso y se tiene que usar desde el mismo
            navegador donde se realiz&oacute; la petici&oacute;n de reestablecimiento de cuenta.
        </p>

    </div>


</div>
</body>
</html>
