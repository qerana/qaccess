<div class="container-fluid" >


    <section class='content'>
        <div class="row">
            <div class="col-xl-7 col-lg-6">
                <div class="card shadow mb-4 ">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between bg-gray-100">
                        <h5 class="text-black">
                            <i class="fa fa-user"></i>
                            <b>Mi perfil</b> 
                        </h5>
                        <div class="dropdown no-arrow">
                            <a href="#" class="btn btn-outline-primary btn-sm"
                               data-target="#modalLg" 
                               data-remote="/welcome/welcome/editprofile/"
                               data-toggle="modal"
                               data-titlemodal='editar mi perfil'
                               >
                                Editar
                            </a>
                            <a href="#" class="btn btn-outline-warning btn-sm"
                               data-target="#modalLg" 
                               data-remote="/welcome/welcome/changemypassword/"
                               data-toggle="modal"
                               data-titlemodal='cambiar mi password'
                               
                               
                               >
                                Cambiar password
                            </a>
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">

                        <div class="container-fluid small" id="data_User" data-User="<?php echo $id_user; ?>">
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-200 p-2">
                                    <b>Estado</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_username">
                                    <span id="detail_title"
                                          class="<?php echo ($User->active == 1) ? 'text-success' : 'text-danger'; ?>"
                                          ><?php echo $User->get_active_status(); ?>  </span>
                                </div>
                            </div>
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>Usuario</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_username">
                                    <?php echo $User->username; ?>
                                </div>
                            </div>
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>Email</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_user_email">
                                    <?php echo $User->user_email; ?> <small class="text-info"><i>Para recuperación de cuenta</i></small>
                                </div>
                            </div>
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>Nombre completo</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_name">
                                    <?php echo $User->get_fullname(); ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-5 col-lg-6">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Perfil asignado</h6>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body small">
                        <div class="row m-1">
                            <div class="col-sm-3 bg-gray-100 p-2">
                                <b>Perfil</b>
                            </div>
                            <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_active">
                                <?php echo $User->Profile->profile_name; ?>
                            </div>
                        </div>
                        <div class="row m-1">
                            <div class="col-sm-3 bg-gray-100 p-2">
                                <b>Layout</b>
                            </div>
                            <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_active">
                                <?php echo $User->Profile->layout; ?>
                            </div>
                        </div>
                        <div class="row m-1">
                            <div class="col-sm-3 bg-gray-100 p-2">
                                <b>Landing</b>
                            </div>
                            <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_active">
                                <?php echo $User->Profile->namespace_landing; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
</div>
<script>


</script>