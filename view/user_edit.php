<div class="modal-body">
    <div class="card shadow mb-4">
        <div class="card-body">
            <form action="/qaccess/User/modify" 
                  id="formQerana" name="formQerana" method="POST" class="form-horizontal"
                  accept-charset="utf-8">
                <input type="hidden" name="f_id_user" id="f_id_user" value="<?php echo $User->id_user; ?>">
                <?php echo $kerana_token; ?>
                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Grabar</button> &nbsp;
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancelar
                    </button>
                </header>
                <div class='form-group form-group-sm row small'> 
                    <label for='f_user_email' class='col-sm-3 col-form-label'>Email <small class="text-info"><i>Para recovery</i></small></label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='email' id='f_user_email' name='f_user_email' 

                                   class='form-control form-control-sm' required   value='<?php echo $User->user_email; ?>' />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_name' class='col-sm-3 col-form-label'>Nombres</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_name' name='f_name' 

                                   class='form-control form-control-sm' required   value='<?php echo $User->name; ?>' />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_lastname' class='col-sm-3 col-form-label'>Apellidos</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_lastname' name='f_lastname' 

                                   class='form-control form-control-sm'    value='<?php echo $User->lastname; ?>' />
                        </div>   
                    </div>   
                </div>   


                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Grabar</button> &nbsp;
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancelar
                    </button>
                </header>

            </form>
        </div>
    </div>
</div>
<script>

// submit form
    $('#formQerana').submit(function (e)
    {
        e.preventDefault();
        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function (data)
            {
                $('#modalLg').modal('hide');
                location.reload();
                
            }
        });


    });


</script>


