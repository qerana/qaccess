<div class="container-fluid" >

    <section class="content-header">
        <h6 class="text-black">
            <a href="/qaccess/User/index" class="btn btn-info btn-circle" title="volver">
                <i class="fa fa-arrow-left"></i>
            </a>
            <b><?php echo $User->username; ?></b> 
        </h6>
    </section> 

    <section class='content'>
        <div class="row">
            <div class="col-xl-8 col-lg-7">
                <div class="card shadow mb-4 ">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between bg-gray-100">
                        <h6 class="m-0"> 

                            <span id="detail_title"
                                  class="<?php echo ($User->active == 1) ? 'text-success' : 'text-danger';?>"
                                  > User <?php echo $User->get_active_status();?>  </span>
                        </h6>
                        <div class="dropdown no-arrow">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="top-end" >
                                <div class="dropdown-header">Opciones:</div>

                                <a href="#" class="dropdown-item"
                                   data-target="#modalLg" 
                                   data-remote="/qaccess/User/edit/<?php echo $id_user; ?>"
                                   data-toggle="modal"
                                   data-titlemodal='editar User'
                                   >
                                    <span class="icon">
                                        <i class="fas fa-edit"></i>
                                    </span>
                                    <span class="text">Editar</span>
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item text-danger" href="/qaccess/User/delete/<?php echo $id_user; ?>">
                                    <span class="icon">
                                        <i class="fas fa-trash"></i>
                                    </span>
                                    <span class="text">Eliminar</span>    
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">

                        <div class="container-fluid small" id="data_User" data-User="<?php echo $id_user; ?>">
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>Username</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_username">
                                    <?php echo $User->username; ?>
                                </div>
                            </div>
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>Email</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_user_email">
                                    <?php echo $User->user_email; ?>
                                </div>
                            </div>
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>Name</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_name">
                                    <?php echo $User->name; ?>
                                </div>
                            </div>
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>Lastname</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_lastname">
                                    <?php echo $User->lastname; ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-5">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Profile:<?php echo $User->Profile->profile_name; ?></h6>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body small">
                        <div class="row m-1">
                            <div class="col-sm-3 bg-gray-100 p-2">
                                <b>Layout</b>
                            </div>
                            <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_active">
                                <?php echo $User->Profile->layout; ?>
                            </div>
                        </div>
                        <div class="row m-1">
                            <div class="col-sm-3 bg-gray-100 p-2">
                                <b>Landing</b>
                            </div>
                            <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_active">
                                <?php echo $User->Profile->namespace_landing; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Group</h6>
                        <div class="dropdown no-arrow">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                                <div class="dropdown-header">Opciones:</div>
                                <a class="dropdown-item" href="#">Opcion1</a>
                                <a class="dropdown-item" href="#">Opcion2</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Otra opcion mas</a>
                            </div>
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                    </div>
                </div>
            </div>
        </div>

    </section>
</div>
<script>


</script>