<div class="container-fluid">

    <section class="content-header">
        <h5 class="text-primary">
            <i class="fa fa-folder fa-2x"></i> User List
        </h5>
        <ol class="breadcrumb">
            <li>
                <input type="text" class="form-control input-sm" 
                       id="filter_search" 
                       name="filter_search"
                       aria-describedby="inputSuccess3Status"
                       value="" placeholder="Buscador"/>
                total:<span class="badge" id="total"></span>
            </li>
        </ol>

    </section> 


    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold">
                <a href="/qaccess/User/add"  class="btn btn-outline-success btn-sm" >
                    <span class="icon">
                        <i class="fas fa-plus-square"></i>
                    </span>
                    <span class="text">Nuevo</span>
                </a>

            </h6>

        </div>
        <!-- Card Body -->
        <div class="card-body">
            <section id='results' class='table-responsive'>
                <div id="loader"></div>
                <table class="table_search table table-bordered table-condensed table-hover">
                    <thead class='bg-gray-600 text-white'>
                        <tr class="small">
                            <td class="bg-gray-800"></td>
                            <td>Username</td>
                            <td>Profile</td>
                            <td>Name</td>
                            <td>Email</td>
                            <td>Active</td>
                        </tr>
                    </thead>
                    <tbody class="small" id="list_User">
                        <?php foreach ($Users AS $User): ?>
                            <tr>
                                <td>
                                    <a href="/qaccess/User/detail/<?php echo $User->id_user; ?>"
                                       class="btn btn-outline-primary btn-sm" title="Detalle User">
                                        <i class="fa fa-tasks"></i>
                                    </a>
                                </td>
                                <td><?php echo $User->username; ?></td>
                                <td><?php echo $User->Profile->profile_name; ?></td>
                                <td><?php echo $User->full_name; ?></td>
                                <td><?php echo $User->user_email; ?></td>
                                <td class="<?php echo ($User->active == 1) ? 'text-success' : 'text-danger';?>"
                                    ><b><?php echo $User->get_active_status(); ?></b></td>
                            </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>
            </section>
        </div>
    </div>

</div>
<script>
//    $(document).ready(function () {
//        loadUser();
//
//
//
//// search html data
//
//
//    });

    $("#filter_search").keyup(function () {
        var filter = $(this).val();

        $("table.table_search > tbody > tr").each(function () {

            // si la lista no existe el string a mostrar, ocultamos las filas
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).hide();

                // Si coincide mostramos el tr
            } else {
                $(this).show();

            }
        });

    });

</script>