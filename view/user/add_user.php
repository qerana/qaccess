<div class="modal-body">
    <div class="card shadow mb-4">
        <div class="card-header">
            <h4>New user</h4>
        </div>
        <div class="card-body">
            <form action="/qaccess/user/save" 
                  id="formQerana" name="formQerana" method="POST" class="form-horizontal"
                  accept-charset="utf-8">

                <?php echo $kerana_token; ?>
                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Grabar</button> &nbsp;
                    <a href="/qaccess/user/index"  class="btn btn-warning btn-sm">
                        Cancelar
                    </a>
                </header>
                <div class='form-group form-group-sm row small'> 
                    <label for='sw_activation_mode' class='col-sm-3 col-form-label'>Activation mode</label>  
                    <div class='col-sm-6'>  
                        <div class='input-group col-sm-8 small'>   

                            <select name="sw_activation_mode" id="sw_activation_mode" 
                                    class="form-control small" required>
                                <option value="">--Activation mode--</option>
                                <option value="direct">Direct</option>
                                <option value="token">Token</option>
                            </select>
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_id_profile' class='col-sm-3 col-form-label'>Profile</label>  
                    <div class='col-sm-6'>  
                        <div class='input-group col-sm-8'>   

                            <select name="f_id_profile" id="f_id_profile" class="form-control" required>
                                <option value="">--Select a profile--</option>
                                <?php foreach ($dependencies['profiles'] AS $Profile): ?>
                                    <option value="<?php echo $Profile->id_profile; ?>">
                                        <?php echo $Profile->profile_name; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_username' class='col-sm-3 col-form-label'>Username</label>  
                    <div class='col-sm-4'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_username' name='f_username' 

                                   class='form-control form-control-sm' required   />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_user_email' class='col-sm-3 col-form-label'>User Email</label>  
                    <div class='col-sm-6'>  
                        <div class='input-group col-sm-8'>   
                            <input type='email' id='f_user_email' name='f_user_email' 

                                   class='form-control form-control-sm' required   />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_name' class='col-sm-3 col-form-label'>Name</label>  
                    <div class='col-sm-4'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_name' name='f_name' 

                                   class='form-control form-control-sm' required   />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_lastname' class='col-sm-3 col-form-label'>Lastname</label>  
                    <div class='col-sm-4'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_lastname' name='f_lastname' 

                                   class='form-control form-control-sm'    />
                        </div>   
                    </div>   
                </div>   
            </form>
        </div>
    </div>
</div>
<script>

// submit form
//    $('#formQerana').submit(function (e)
//    {
//        e.preventDefault();
//        var form = $(this);
//        var url = form.attr('action');
//
//        $.ajax({
//            type: "POST",
//            url: url,
//            data: form.serialize(), // serializes the form's elements.
//            success: function (data)
//            {
//                $('#modalLg').modal('hide');
//                loadUser();
//            }
//        });
//
//
//    });


</script>


