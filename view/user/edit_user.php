<div class="modal-body">
    <div class="card shadow mb-4">
        <div class="card-body">
            <form action="/qaccess/User/modify" 
                  id="formQerana" name="formQerana" method="POST" class="form-horizontal"
                  accept-charset="utf-8">
                <input type="hidden" name="f_id_user" id="f_id_user" value="<?php echo $User->id_user; ?>">
                <?php echo $kerana_token; ?>
                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Grabar</button> &nbsp;
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancelar
                    </button>
                </header>
                <div class='form-group form-group-sm row small'> 
                    <label for='f_id_profile' class='col-sm-3 col-form-label'>Profile</label>  
                    <div class='col-sm-6'>  
                        <div class='input-group col-sm-8'>   

                            <select name="f_id_profile" id="f_id_profile" class="form-control" required>
                                <?php foreach ($dependencies['profiles'] AS $Profile): ?>
                                    <option value="<?php echo $Profile->id_profile; ?>"
                                    <?php echo ($User->id_profile == $Profile->id_profile) ? 'selected' : ''; ?>
                                            >
                                                <?php echo $Profile->profile_name; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_username' class='col-sm-3 col-form-label'>Username</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_username' name='f_username' disabled

                                   class='form-control form-control-sm' required   value='<?php echo $User->username; ?>' />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_user_email' class='col-sm-3 col-form-label'>User Email</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='email' id='f_user_email' name='f_user_email' 

                                   class='form-control form-control-sm' required   value='<?php echo $User->user_email; ?>' />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_name' class='col-sm-3 col-form-label'>Name</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_name' name='f_name' 

                                   class='form-control form-control-sm' required   value='<?php echo $User->name; ?>' />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_lastname' class='col-sm-3 col-form-label'>Lastname</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_lastname' name='f_lastname' 

                                   class='form-control form-control-sm'    value='<?php echo $User->lastname; ?>' />
                        </div>   
                    </div>   
                </div>   

                <div class='form-group form-group-sm row small'> 
                    <label for='f_active' class='col-sm-3 col-form-label'>Active</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-12'>   
                            <div class="form-check">
                                <input class="form-check-input" name='f_active' 
                                       type="radio" value="1" id="active"
                                       <?php echo ($User->active == 1) ? 'checked' : ''; ?>
                                       >
                                <label class="form-check-label" for="defaultCheck1">
                                    Active
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name='f_active'
                                       value="0" id="inactive"
                                       <?php echo ($User->active == 0) ? 'checked' : ''; ?>>
                                <label class="form-check-label" for="defaultCheck1">
                                    Inactive
                                </label>
                            </div>

                        </div>   
                    </div>   
                </div>   

                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Grabar</button> &nbsp;
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancelar
                    </button>
                </header>

            </form>
        </div>
    </div>
</div>
<script>

//// submit form
//    $('#formQerana').submit(function (e)
//    {
//        e.preventDefault();
//        var form = $(this);
//        var url = form.attr('action');
//
//        $.ajax({
//            type: "POST",
//            url: url,
//            data: form.serialize(), // serializes the form's elements.
//            success: function (data)
//            {
//                $('#modalLg').modal('hide');
//                
//            }
//        });
//
//
//    });


</script>


