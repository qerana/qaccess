
    <html>
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <title><?php echo __APPNAME__; ?> User-activation </title>

            <!-- Custom fonts for this template-->
            <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

            <!-- Custom styles for this template-->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">


            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

            <!-- Google Font -->
            <script>



            </script>
        </head>

        <body class="bg-gray-100">


            <div class="container">

                <!-- Outer Row -->
                <div class="row justify-content-center">

                    <div class="card o-hidden border-0 shadow-lg my-5">
                        <div class="card-body p-0">
                            <!-- Nested Row within Card Body -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="p-5">
                                        <div class="text-center">
                                            <h3 class="sidebar-brand-icon text-danger">
                                                Recuperaci&oacute;n de cuenta
                                            </h3>
                                            <div class="sidebar-brand-text mx-3 text-gray-900">
                                                Escribe tu email asociado a tu cuenta y te enviaremos instrucciones
                                                de como recuperar tu cuenta.

                                            </div>
                                            <br>
                                        </div>
                                        <form action="/welcome/welcome/dorecovery" class="user" method='post'>
                                            <div class="form-group">
                                                <div class="col col-sm-8">
                                                    <input type="email" id="f_user_email" name="f_user_email" 
                                                           class="form-control form-control-user"  
                                                           required 
                                                           title="Escribe un correo electronico valido" placeholder="tu email..."/>

                                                </div>
                                            </div>
                                            <button type='submit' class="btn btn-primary btn-user btn-block" >
                                                Recuperar cuenta
                                            </button>

                                        </form>
                                        <hr>
                                        <div class="text-center">
                                            <div class="alert alert-primary">
                                                For security reasons your IP address ( <strong><?php echo filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_SANITIZE_SPECIAL_CHARS); ?></strong>) 
                                                as been stored.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </body>
    </html>