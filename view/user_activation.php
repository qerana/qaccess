<?php if (isset($_SESSION['S_activation_tokenid']) AND isset($_SESSION['S_token_id_user'])) {
    ?>

    <html>
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <title><?php echo __APPNAME__; ?> User-activation </title>

            <!-- Custom fonts for this template-->
            <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

            <!-- Custom styles for this template-->
            <link href="/_styles/sbadmin2/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
            <!-- Custom styles for this template-->
            <link href="/_styles/sbadmin2/css/sb-admin-2.min.css" rel="stylesheet">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <style>
                .bg-login-arandu {
                    background: url('/src/img/welcome-login.jpg');
                    background-position: center;
                    background-size: cover;
                }
            </style>
            <script>


                $("#formChangePassword").submit(function () {

                    var pass0 = $('#f_password').val();
                    var pass1 = $('#f_password1').val();
                    if (pass0 !== pass1) {
                        $('#div_error_password').removeClass('hidden');
                        $('#div_error_password').html('Las contraseñas no coinciden');
                        return false;
                    } else {
                        return true;
                    }

                });


                /**
                 * -------------------------------------------------------------------------
                 * Toogle password type
                 * -------------------------------------------------------------------------
                 * @returns {undefined}
                 */
                function tooglePassword() {

                    if ($('#f_password').prop('type') === 'password') {
                        $('#f_password').prop('type', 'text');
                    } else {
                        $('#f_password').prop('type', 'password');
                    }
                    if ($('#f_password_repeated').prop('type') === 'password') {
                        $('#f_password_repeated').prop('type', 'text');
                    } else {
                        $('#f_password_repeated').prop('type', 'password');
                    }


                }

            </script>
        </head>

        <body class="bg-gray-100">


            <div class="container">

                <!-- Outer Row -->
                <div class="row justify-content-center">

                    <div class="card o-hidden border-0 shadow-lg my-5">
                        <div class="card-body p-0">
                            <!-- Nested Row within Card Body -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="p-5">
                                        <div class="text-center">
                                            <div class="sidebar-brand-icon">
                                                <h2 class="text-black1">
                                                    <i class="fas fa-user-lock fa-2x text-warning"></i>  <?php echo __APPNAME__; ?>
                                                </h2>
                                            </div>
                                            <h5 class="text-gray-600">User activation</h5>
                                        </div>
                                        <div class="text-center">
                                            <div class="sidebar-brand-text mx-3 text-info">
                                                Solo un paso mas, escribe una contraseña segura y tu cuenta estará activada.

                                            </div>
                                            <br>
                                        </div>
                                         <div class="alert alert-info" role="alert">
                                            La contrase&ntilde;a debe cumplir con los siguientes requisitos
                                        </div>
                                        <ul class="small">
                                            <li class="text-dark" id="min8">M&iacute;nimo 8 caracteres</li>
                                            <li class="text-dark">M&iacute;nimo 1 n&uacute;mero</li>
                                            <li class="text-dark">M&iacute;nimo 1 letra en may&uacute;sculas</li>
                                            <li class="text-dark">M&iacute;nimo 1 letra en min&uacute;sculas</li>
                                        </ul>
                                        <div class="card-body">
                                            <div class="progress">
                                                <div id="result" class="progress-bar progress-bar-striped text-gray-900" 
                                                     role="progressbar" style="width: 0%" 
                                                     aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">
                                                </div>
                                            </div>
                                        </div>
                                        <form action="/welcome/welcome/activate" class="user" method='post'>
                                            <div class="form-group">
                                                <label for='f_name' class='col-sm-4 control-label'>Password
                                                    <span class="input-group-addon"><i onclick="tooglePassword()" class="fa fa-eye"></i></span>
                                                </label>
                                                <div class="col col-sm-8">
                                                    <input type="password" id="f_password" name="f_password" 
                                                           class="form-control form-control-user"  
                                                           pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                                                           required 
                                                           title="Debe tener almenos un numero, una letra en MAY y otra en MIN, y por lo
                                                           menos 8 caracteres"/>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for='f_name' class='col-sm-4 control-label'>Repite Password
                                                    <span class="input-group-addon"><i onclick="tooglePassword()" class="fa fa-eye"></i></span>
                                                </label> 
                                                <div class="col col-sm-8">
                                                    <input type="password" id="f_password_repeated" name="f_password_repeated" 
                                                           class="form-control form-control-user"  
                                                           pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                                                           required 
                                                           title="Debe tener almenos un numero, una letra en MAY y otra en MIN, y por lo
                                                           menos 8 caracteres"/>
                                                </div>
                                            </div>
                                            <button type='submit' class="btn btn-success btn-user btn-block" >
                                                Activar cuenta
                                            </button>

                                        </form>
                                        <hr>
                                        <div class="text-center">
                                            <div class="alert alert-primary">
                                                For security reasons your IP address ( <strong><?php echo filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_SANITIZE_SPECIAL_CHARS); ?></strong>) 
                                                as been stored.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

            </div>

            <!-- /.login-box -->

            <!-- Bootstrap core JavaScript-->
            <script src="/_styles/sb_admin2/vendor/jquery/jquery.min.js"></script>
            <script src="/_styles/sb_admin2/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

            <!-- Core plugin JavaScript-->
            <script src="/_styles/sb_admin2/vendor/jquery-easing/jquery.easing.min.js"></script>

            <!-- Custom scripts for all pages-->
            <script src="/_styles/sb_admin2/js/sb-admin-2.min.js"></script>
             <script>
                                                        $(document).ready(function () {
                                                            $('#f_password').keyup(function () {
                                                                $('#result').html(checkStrength($('#f_password').val()));
                                                            });
                                                            function checkStrength(password) {
                                                                var strength = 0;
                                                                if (password.length < 6) {
                                                                    $('#result').removeClass();
                                                                    $('#result').addClass('progress-bar progress-bar-striped bg-warning');
                                                                    $("#result").css({"width": "10%"});
                                                                    return 'Muy corto';
                                                                }
                                                                if (password.length > 7)
                                                                    strength += 1;
    // If password contains both lower and uppercase characters, increase strength value.
                                                                if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
                                                                    strength += 1;
    // If it has numbers and characters, increase strength value.
                                                                if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
                                                                    strength += 1;
    // If it has one special character, increase strength value.
                                                                if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
                                                                    strength += 1;
    // If it has two special characters, increase strength value.
                                                                if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
                                                                    strength += 1;
    // Calculated strength value, we can return messages
    // If value is less than 2
                                                                if (strength < 2) {
                                                                    $('#result').removeClass();
                                                                    $('#result').addClass('progress-bar progress-bar-striped bg-danger');
                                                                    $("#result").css({"width": "15%"});
                                                                    return 'Débil';
                                                                } else if (strength === 2) {
                                                                    $('#result').removeClass();
                                                                    $('#result').addClass('progress-bar progress-bar-striped bg-info');
                                                                    $("#result").css({"width": "70%"});
                                                                    return 'Casi pero no';
                                                                } else {
                                                                    $('#result').removeClass();
                                                                    $('#result').addClass('progress-bar progress-bar-striped bg-success');
                                                                    $("#result").css({"width": "95%"});
                                                                    return 'Fuerte';
                                                                }
                                                            }
                                                        });


                                                        /**
                                                         * -------------------------------------------------------------------------
                                                         * Toogle password type
                                                         * -------------------------------------------------------------------------
                                                         * @returns {undefined}
                                                         */
                                                        function tooglePassword() {

                                                            if ($('#f_password').prop('type') === 'password') {
                                                                $('#f_password').prop('type', 'text');
                                                            } else {
                                                                $('#f_password').prop('type', 'password');
                                                            }
                                                            if ($('#f_password_verification').prop('type') === 'password') {
                                                                $('#f_password_verification').prop('type', 'text');
                                                            } else {
                                                                $('#f_password_verification').prop('type', 'password');
                                                            }


                                                        }



            </script>
        </body>
    </html>
    <?php
} else {
    die("Invalid Access!!");
}
