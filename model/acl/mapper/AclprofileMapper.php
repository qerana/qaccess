<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model\acl\mapper;

use 
    Ada\adapters\AdapterInterface,
    Ada\adapters\PDOAdapter,
    Ada\mapper\AdaDataMapper,
    Qerapp\qaccess\model\acl\entity\AclprofileEntity,
    Qerapp\qaccess\model\acl\interfaces\AclprofileMapperInterface,
    Qerapp\qaccess\model\acl\interfaces\AclprofileInterface;
    
//RELATED-NAMESPACES    
    
/*
  |*****************************************************************************
  | MAPPER CLASS for AclprofileMapperEntity
  |*****************************************************************************
  |
  | MAPPER AclprofileMapper
  | @author qDevTools,
  | @date 2020-10-31 20:16:48,
  |*****************************************************************************
 */

class AclprofileMapper extends AdaDataMapper implements AclprofileMapperInterface  {

    //RELATED-MAPPERS
    
     public function __construct(AdapterInterface $Adapter = null)
    {

          $this->_Adapter = (is_null($Adapter) ) ? new PDOAdapter(\Ada\SqlPDO::singleton(),'qer_profile_acl') : $Adapter;
         
         //RELATED-MAPPER-OBJECT
         
         
        parent::__construct($this->_Adapter);
        
    }
    
    
    /*
     * -------------------------------------------------------------------------
     * Find by id
     * -------------------------------------------------------------------------
     * @param int $id
     * @return EntityObject
     */
    public function findById(int $id)
    {
        $row = $this->_Adapter->find(['id_profile' => $id], ['fetch' => 'one']);

        // if row exists , the create a new entity, otherwise null is returned
        if (!$row) {
            return null;
        } else {
            return $this->createEntity($row);
        }
    }
    
    /**
     * -------------------------------------------------------------------------
     * Save entity
     * -------------------------------------------------------------------------
     */
    public function save(AclprofileInterface $Aclprofile){
        
        $data = parent::getDataObject($Aclprofile);
        
        $this->_Adapter->insert($data);
        
    }
    
     /**
     * -------------------------------------------------------------------------
     * Delete entity
     * -------------------------------------------------------------------------
     * @param mixed $id
     * @return type
     */
    public function delete()
    {

    }
    
        /**
     * -------------------------------------------------------------------------
     * Create Entity
     * -------------------------------------------------------------------------
     * @param array $row
     * @return Entity
     */
    protected function createEntity(array $row): AclprofileEntity
    {
        $AclprofileEntity = new AclprofileEntity($row);
        return $AclprofileEntity;
    }
 
 
}
