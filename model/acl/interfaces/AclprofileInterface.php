<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model\acl\interfaces;

/*
  |*****************************************************************************
  | INTERFACE  for [{folder_name}]
  |*****************************************************************************
  |
  | Entity [{folder_name}]
  | @author qDevTools,
  | @date 2020-10-31 20:16:48,
  |*****************************************************************************
 */

interface AclprofileInterface {

//SETTERS

    public function set_id_profile(int $id_profile): void;

    public function set_id_profile_acl(int $id_profile_acl): void;

    public function set_module(string $module): void;

    public function set_nav_content(string $nav_content): void;

    public function set_controller(string $controller): void;

    public function set_action(string $action): void;

//GETTERS

    public function get_id_profile();

    public function get_id_profile_acl();

    public function get_module();
    
    public function get_nav_content();

    public function get_controller();

    public function get_action();
}
