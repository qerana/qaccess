<?php

/*
 * Copyright (C) 2019/20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model\acl\entity;

use \Ada\EntityManager,
    Qerapp\qaccess\model\acl\interfaces\AclprofileInterface;

//RELATED-NS


/*
  |*****************************************************************************
  | ENTITY CLASS for Aclprofile
  |*****************************************************************************
  |
  | Entity Aclprofile
  | @author qDevTools,
  | @date 2020-10-31 20:16:48,
  |*****************************************************************************
 */

class AclprofileEntity extends EntityManager implements AclprofileInterface, \JsonSerializable {

    protected

    //ATRIBUTES        

    /** @var text(), $nav_content  */
            $_nav_content,
            /** @var int(11), $id_profile_acl  */
            $_id_profile_acl,
            /** @var int(11), $module_order  */
            $_module_order,
            /** @var int(11), $id_profile  */
            $_id_profile,
            /** @var varchar(100), $module  */
            $_module,
            /** @var varchar(100), $controller  */
            $_controller,
            /** @var varchar(100), $action  */
            $_action;

    //RELATED

    public function __construct(array $data = []) {
        $this->populate($data);

        //RELATED-LOAD
    }

//SETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Setter for nav_content
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_nav_content($nav_content = ""): void {
        $this->_nav_content = $nav_content;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_profile_acl
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_profile_acl($id_profile_acl = 0): void {
        $this->_id_profile_acl = filter_var($id_profile_acl, FILTER_SANITIZE_STRING);
    }


    /**
     * ------------------------------------------------------------------------- 
     * Setter for module_order
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_module_order($module_order = 0): void {
        $this->_module_order = filter_var($module_order, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_profile
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_profile($id_profile = 0): void {
        $this->_id_profile = filter_var($id_profile, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for module
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_module($module = ""): void {
        $this->_module = filter_var($module, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for controller
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_controller($controller = ""): void {
        $this->_controller = filter_var($controller, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for action
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_action($action = ""): void {
        $this->_action = filter_var($action, FILTER_SANITIZE_STRING);
    }

//GETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Getter for nav_content
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_nav_content() {
        return $this->_nav_content;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_profile_acl
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_profile_acl() {
        return $this->_id_profile_acl;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for module_order
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_module_order() {
        return $this->_module_order;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_profile
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_profile() {
        return $this->_id_profile;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for module
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_module() {
        return filter_var($this->_module, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for controller
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_controller() {
        return filter_var($this->_controller, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for action
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_action() {
        return filter_var($this->_action, FILTER_SANITIZE_STRING);
    }

}
