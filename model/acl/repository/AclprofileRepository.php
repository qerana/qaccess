<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model\acl\repository;
use Qerapp\qaccess\model\acl\interfaces\AclprofileMapperInterface,
Qerapp\qaccess\model\acl\interfaces\AclprofileRepositoryInterface,
Qerapp\qaccess\model\acl\interfaces\AclprofileInterface;
/*
  |*****************************************************************************
  | AclprofileRepositoryRepository
  |*****************************************************************************
  |
  | Repository AclprofileRepository
  | @author qDevTools,
  | @date 2020-10-31 20:16:48,
  |*****************************************************************************
 */

class AclprofileRepository implements AclprofileRepositoryInterface
{
    
    private
            $_AclprofileMapper;
            
    public function __construct(AclprofileMapperInterface $Mapper)
    {
        
         $this->_AclprofileMapper = $Mapper;
        
    }
          /**
     * -------------------------------------------------------------------------
     * Get One
     * -------------------------------------------------------------------------
     * @return AclprofileRepository
     */
    
     public function find(array $condition = [],array $options = []) {
        return $this->_AclprofileMapper->findOne($condition,$options);
    }
    
    
      /**
     * -------------------------------------------------------------------------
     * Get all AclprofileRepository
     * -------------------------------------------------------------------------
     * @return AclprofileRepositoryEntity collection
     */
    
    public function findById(int $id)
    {
        return $this->_AclprofileMapper->findOne(['id_profile'=>$id]);
    }
    
     /**
     * -------------------------------------------------------------------------
     * Get all AclprofileRepository
     * -------------------------------------------------------------------------
     * @return AclprofileRepositoryEntity collection
     */
    
    public function findAll(array $conditions = [],array $options = [])
    {
        return $this->_AclprofileMapper->findAll($conditions,$options);
    }
    
    
    /** 
* ------------------------------------------------------------------------- 
* Fin by  id_profile
* ------------------------------------------------------------------------- 
* @param id_profile 
*/ 
  public function findById_profile(int  $id_profile,array $options = [])
{ 
return $this->_AclprofileMapper->findAll(['id_profile'=> $id_profile],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  module
* ------------------------------------------------------------------------- 
* @param module 
*/ 
  public function findByModule(string $module,array $options = [])
{ 
return $this->_AclprofileMapper->findAll(['module'=> $module],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  controller
* ------------------------------------------------------------------------- 
* @param controller 
*/ 
  public function findByController(string $controller,array $options = [])
{ 
return $this->_AclprofileMapper->findAll(['controller'=> $controller],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  action
* ------------------------------------------------------------------------- 
* @param action 
*/ 
  public function findByAction(string $action,array $options = [])
{ 
return $this->_AclprofileMapper->findAll(['action'=> $action],$options);
}
    
   
    
     /**
     * -------------------------------------------------------------------------
     * Save AclprofileRepository
     * -------------------------------------------------------------------------
     * @object $AclprofileRepositoryEntity
     * @return type
     */
    public function store(AclprofileInterface $AclprofileRepositoryEntity)
    {
        return $this->_AclprofileMapper->save($AclprofileRepositoryEntity);
    }
    
     /**
     * -------------------------------------------------------------------------
     * Delete AclprofileRepository
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function remove($id)
    {
        return $this->_AclprofileMapper->delete($id);
    }
 
}
