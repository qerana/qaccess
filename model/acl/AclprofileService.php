<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model\acl;

use Qerapp\qaccess\model\acl\entity\AclprofileEntity,
    Qerapp\qaccess\model\acl\interfaces\AclprofileInterface,
    Qerapp\qaccess\model\acl\mapper\AclprofileMapper,
    Qerapp\qaccess\model\acl\interfaces\AclprofileMapperInterface,
    Qerapp\qaccess\model\acl\repository\AclprofileRepository;

//RELATED-MAPPERS

/*
  |*****************************************************************************
  | [{server_name}]
  |*****************************************************************************
  |
  | Service for Entity AclprofileService
  | @author qDevTools,
  | @date 2020-10-31 20:16:48,
  |*****************************************************************************
 */

class AclprofileService {

    private
            $id_profile,
            $AclProfileMapper;
    public

    //RELATED-MAPPER-OBJECT
            $AclprofileRepository,
            /** @object entity Aclprofile */
            $Aclprofile;

    public function __construct(AclprofileMapperInterface $Mapper = null) {

        //RELATED-MAPPER-OBJECT-NEW

        try {
            $AclprofileMapper = new AclprofileMapper;
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException('Mapper.AclprofileService', $ex);
        }

        $this->AclProfileMapper = (is_null($Mapper)) ? $AclprofileMapper : $Mapper;
    }

    /**
     * check level one
     * @param string $module
     */
    public function checkLevelOne(string $module) {

        $this->id_profile = $_SESSION['Q_id_profile'];

        $this->AclprofileRepository = new AclprofileRepository($this->AclProfileMapper);

        $conditions = [
            'id_profile' => $this->id_profile,
            'module' => $module
        ];

        $LevelOne = $this->AclprofileRepository->find($conditions, ['fetch' => 'one']);

        if (!$LevelOne instanceof AclprofileInterface) {
            \QException\Exceptions::showHttpStatus(403, 'ACL-Level-1 denied access to MODULE ' . $module . ' to USER ' . $_SESSION['Q_username']);
        }
    }

    /**
     * check level 2
     * @param string $module
     * @param string $controller
     */
    public function checkLevelTwo(string $module, string $controller) {

        $this->id_profile = $_SESSION['Q_id_profile'];

        $this->AclprofileRepository = new AclprofileRepository($this->AclProfileMapper);

        $conditions = [
            'id_profile' => $this->id_profile,
            'module' => $module,
            'controller' => $controller
        ];

        $LevelOne = $this->AclprofileRepository->find($conditions, ['fetch' => 'one']);

        if (!$LevelOne instanceof AclprofileInterface) {
            \QException\Exceptions::showHttpStatus(403, 'ACL-Level-2 denied access to MODULE ' . $module . ',CONTROLLER ' . $controller . ' to USER ' . $_SESSION['Q_username']);
        }
    }

    /**
     *  Get modules profile
     */
    public function getModulesProfile() {
        
        $this->id_profile = $_SESSION['Q_id_profile'];
        $this->AclprofileRepository = new AclprofileRepository($this->AclProfileMapper);
        $Modules = $this->AclprofileRepository->findAll(
                ['id_profile'=>$this->id_profile,'controller'=> '0'],
                ['orderby'=>'module_order','groupby'=>'module']);
        
        $_SESSION['navmap_modules'] = $Modules;
    }

    /**
     * -------------------------------------------------------------------------
     * Get all
     * -------------------------------------------------------------------------
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getAll($json = false) {

        $Collection = $this->AclprofileRepository->findAll();
        if ($json) {
            echo json_encode($Collection);
        } else {
            return $Collection;
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Get by id
     * -------------------------------------------------------------------------
     * @param $id , id entity
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getById(int $id, $json = false) {

        $this->Aclprofile = $this->AclprofileRepository->findById($id);
        if ($this->Aclprofile) {
            if ($json) {
                echo json_encode($this->Aclprofile);
            } else {
                return $this->Aclprofile;
            }
        } else {
            \QException\Exceptions::showHttpStatus(404, 'Aclprofile ' . $id . ' NOT FOUND!!');
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Save  array data
     * -------------------------------------------------------------------------
     */
    public function save(array $data = []) {

        $this->createAclprofile($data);
        $this->storeAclprofile($this->Aclprofile);
    }

    /**
     * -------------------------------------------------------------------------
     * Save  Object
     * -------------------------------------------------------------------------
     */
    public function storeAclprofile($Aclprofile) {

        $this->AclprofileRepository->store($Aclprofile);
    }

    /**
     *  Create empleado entity
     * @param array $data
     */
    public function createAclprofile(array $data = []) {

        $data_to_save = (empty($data)) ? \helpers\Request::getFormData() : $data;
        $this->Aclprofile = new AclprofileEntity($data_to_save);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function delete(int $id) {
        return $this->AclprofileRepository->remove($id);
    }

}
