<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Qerapp\qaccess\model\acl;

/**
 * *****************************************************************************
 * Description of AclCliqer
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class AclCliqer extends AclService {

    protected
            $_options;

    public function __construct(array $options) {
        parent::__construct();
        $this->_options = $options;
    }

    /**
     * -------------------------------------------------------------------------
     * Add a url to a user
     * -------------------------------------------------------------------------
     */
    public function add() {
        // check if all is setted
        if ($this->_options['user'] == '@all') {
            die("sisi");
        }

        // check url components
        $url = trim(filter_var($this->_options['url'], FILTER_SANITIZE_URL), '/');
        $url_components = explode('/', $url);

        $num_components = count($url_components);
        //if url has 3 parts then add url
        if ($num_components == 3) {
            $this->_options += ['active' => 'yes'];
            if (parent::save($this->_options)) {
                echo "\n------ACL created-------\n";
            }
        }
        // if has only 2 then add typical methods to acl
        else if ($num_components == 2) {

            $acl_data = [
                'user' => $this->_options['user'],
                'active' => yes
            ];

            $acl_index = $acl_data;
            $acl_index += ['url' => '/' . $url . '/index'];
            $acl_getall = $acl_data;
            $acl_getall += ['url' => '/' . $url . '/getAllInJson'];
            $acl_getone = $acl_data;
            $acl_getone += ['url' => '/' . $url . '/getOneInJson'];
            $acl_add = $acl_data;
            $acl_add += ['url' => '/' . $url . '/add'];
            $acl_save = $acl_data;
            $acl_save += ['url' => '/' . $url . '/save'];
            $acl_detail = $acl_data;
            $acl_detail += ['url' => '/' . $url . '/detail'];
            $acl_edit = $acl_data;
            $acl_edit += ['url' => '/' . $url . '/edit'];
            $acl_modify = $acl_data;
            $acl_modify += ['url' => '/' . $url . '/modify'];
            $acl_delete = $acl_data;
            $acl_delete += ['url' => '/' . $url . '/delete'];

            parent::save($acl_index);
            parent::save($acl_getall);
            parent::save($acl_getone);
            parent::save($acl_add);
            parent::save($acl_save);
            parent::save($acl_detail);
            parent::save($acl_edit);
            parent::save($acl_modify);
            parent::save($acl_delete);
        }
        $output = '============================================================' . "\n";
        $output .= " Qaccess: rule successfull enabled \n";
        $output .= '-----------------------------------------------------------' . "\n";
        $output .= " user= " . $this->_options['user'] . " \n";
        $output .= " url= " . $this->_options['url'] . " \n";

        $output .= '============================================================' . "\n";

        echo $output;
    }

}
