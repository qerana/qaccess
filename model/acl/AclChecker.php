<?php
/*
 * This file is part of QeranaProject
 * Copyright (C) 2020-21  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Qerapp\qaccess\model\acl;

use Qerapp\qaccess\model\acl\AclprofileService;

/**
 * Description of AclChecker
 *
 * @author diemarc
 */
class AclChecker {

    public
            
            $controller,
            $module,
            $action;
    private
            $acl_level,
            $id_profile,
            $AclProfileService;

    public function __construct() {
        $this->id_profile = $_SESSION['Q_id_profile'];
        $this->acl_level = $_SESSION['Q_levelacl'];
        $this->AclProfileService = new AclprofileService;
        
    }

    public function check() {


        try {
            // check acl level access
            switch ($this->acl_level):

                case '1';
                    $this->checkLevelOne();
                    break;

                case '2';
                    $this->checkLevelTwo();
                    break;

                case '3';
                    $this->checkLevelThree();
                    break;

            endswitch;
        } catch (\Throwable $ex) {
            \QException\Exceptions::ShowException('Security.Acl.Checker.#Num:'.rand(), $ex);
        }
    }

    /**
     * Check level one
     * @return type
     */
    private function checkLevelOne() {

        if (empty($this->module)) {
            \QException\Exceptions::showError('Security.ACL.Level1', 'Module, not setted!');
        }

        return $this->AclProfileService->checkLevelOne($this->module);
    }
    
    /**
     * Check level two
     * @return type
     */
    private function checkLevelTwo() {

        if (empty($this->module) OR (empty($this->controller))) {
            \QException\Exceptions::showError('Security.ACL.Level2', 'Module or Controller not setted!');
        }

        return $this->AclProfileService->checkLevelTwo($this->module,$this->controller);
    }
    

}
