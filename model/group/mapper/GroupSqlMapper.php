<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\group\mapper;
use Ada\adapters\PDOAdapter,
    Ada\mapper\AdaDataMapper,
    Qerapp\qbasic\model\group\entity\GroupEntity,
    Qerapp\qbasic\model\group\entity\GroupInterface;
/*
  |*****************************************************************************
  | MAPPER CLASS for GroupEntity
  |*****************************************************************************
  |
  | MAPPER Group
  | @author TUPA,
  | @date 2019-08-23 07:42:09,
  |*****************************************************************************
 */

class GroupSqlMapper extends AdaDataMapper implements GroupMapperInterface  {

    
     public function __construct()
    {
         
        parent::__construct(new PDOAdapter(\Ada\SqlPDO::singleton(),'qer_group'));
        
    }
    
    
    /*
     * -------------------------------------------------------------------------
     * Find by id
     * -------------------------------------------------------------------------
     * @param int $id
     * @return EntityObject
     */
    public function findById(int $id)
    {
        $row = $this->_Adapter->find(['id_group' => $id], ['fetch' => 'one']);

        // if row exists , the create a new entity, otherwise null is returned
        if (!$row) {
            return null;
        } else {
            return $this->createEntity($row);
        }
    }
    
    /**
     * -------------------------------------------------------------------------
     * Save entity
     * -------------------------------------------------------------------------
     */
    public function save(GroupInterface $Group){
        
        $data = parent::getDataObject($Group);
        
        if (is_null($Group->id_group)) {
            $Group->id_group = $this->_Adapter->insert($data);
        } else {
           $this->_Adapter->update($data, ['id_group' => $Group->id_group]);
        }
        
    }
    
     /**
     * -------------------------------------------------------------------------
     * Delete entity
     * -------------------------------------------------------------------------
     * @param mixed $id
     * @return type
     */
    public function delete($id)
    {

        // if $id is a object and a UserEntity  
        if ($id instanceof GroupInterface) {
            $id = $id->id;
        }

        return $this->_Adapter->delete(['id' => $id]);
    }
    
        /**
     * -------------------------------------------------------------------------
     * Create Entity
     * -------------------------------------------------------------------------
     * @param array $row
     * @return Entity
     */
    protected function createEntity(array $row): GroupEntity
    {
        
        return  new GroupEntity($row);
    }
 
 
}