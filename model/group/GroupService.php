<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\group;
use Qerapp\qbasic\model\group\entity\GroupEntity AS GroupEntity,
        Qerapp\qbasic\model\group\mapper\GroupSqlMapper AS GroupMapper,
        Qerapp\qbasic\model\group\mapper\GroupMapperInterface,
        Qerapp\qbasic\model\group\repository\GroupRepository AS GroupRepository;
/*
  |*****************************************************************************
  | GroupService
  |*****************************************************************************
  |
  | Service for Entity Group
  | @author TUPA,
  | @date 2019-08-23 07:42:09,
  |*****************************************************************************
 */

class GroupService 
{
    
    protected
            $_GroupRepository;
            
    public function __construct(GroupMapperInterface $Mapper = null)
    {
         $MapperRepository = (is_null($Mapper)) ? new GroupMapper() : $Mapper;
         $this->_GroupRepository = new GroupRepository($MapperRepository);
        
    }
    
    /**
     * -------------------------------------------------------------------------
     * Get all
     * -------------------------------------------------------------------------
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getAll($json = false){
        
        $Collection = $this->_GroupRepository->findAll(); 
        return ($json === true) ? \helpers\Utils::parseToJson($Collection) :$Collection; 
        
    }
    
     /**
     * -------------------------------------------------------------------------
     * Get by id
     * -------------------------------------------------------------------------
     * @param $id , id entity
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getById(int $id,$json = false){
        
        $Entity = $this->_GroupRepository->findById($id);
        return ($json === true) ? \helpers\Utils::parseToJson($Entity) : $Entity;
        
    }
    
     /**
     * -------------------------------------------------------------------------
     * Save 
     * -------------------------------------------------------------------------
     */
   
    public function save(array $data = []){
        
        $data_to_save = (empty($data)) ? \helpers\Request::getFormData() : $data;
        
        $Group = new GroupEntity($data_to_save);
        $this->_GroupRepository->store($Group);
        
        
    }
    
 
}