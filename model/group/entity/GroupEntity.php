<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\group\entity;

use \Ada\EntityManager;

/*
  |*****************************************************************************
  | ENTITY CLASS for Group
  |*****************************************************************************
  |
  | Entity Group
  | @author TUPA,
  | @date 2019-08-23 07:42:09,
  |*****************************************************************************
 */

class GroupEntity extends EntityManager implements GroupInterface
{

    //ATRIBUTES
    protected
    /** @var int(11), $id_group  */
            $_id_group,
            /** @var varchar(45), $group_name  */
            $_group_name;

    public function __construct(array $data = [])
    {
        $this->populate($data);
    }

//SETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_group
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_group($id_group): void
    {
        $this->_id_group = $id_group;
    }

/**
     * ------------------------------------------------------------------------- 
     * Setter for group_name
     * ------------------------------------------------------------------------- 
     * @param string 
     */

    public function set_group_name($group_name): void
    {
        $this->_group_name = $group_name;
    }

//GETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_group
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_group()
    {
        return $this->_id_group;
    }

/**
     * ------------------------------------------------------------------------- 
     * Getter for group_name
     * ------------------------------------------------------------------------- 
     * @return string  
     */

    public function get_group_name()
    {
        return $this->_group_name;
    }

}
