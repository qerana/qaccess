<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\group\repository;
use Qerapp\qbasic\model\group\mapper\GroupMapperInterface,
        Qerapp\qbasic\model\group\entity\GroupInterface;
/*
  |*****************************************************************************
  | GroupRepository
  |*****************************************************************************
  |
  | Repository Group
  | @author TUPA,
  | @date 2019-08-23 07:42:09,
  |*****************************************************************************
 */

class GroupRepository implements GroupRepositoryInterface
{
    
    private
            $_GroupMapper;
            
    public function __construct(GroupMapperInterface $Mapper)
    {
        
         $this->_GroupMapper = $Mapper;
        
    }
    
    /** 
* ------------------------------------------------------------------------- 
* Fin by  id_group
* ------------------------------------------------------------------------- 
* @param id_group 
*/ 
  public function findById_group(int  $id_group,array $options = [])
{ 
return $this->_GroupMapper->findAll(["id_group"=> $id_group],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  group_name
* ------------------------------------------------------------------------- 
* @param group_name 
*/ 
  public function findByGroup_name(string $group_name,array $options = [])
{ 
return $this->_GroupMapper->findAll(["group_name"=> $group_name],$options);
}
    
     /**
     * -------------------------------------------------------------------------
     * Get all Group
     * -------------------------------------------------------------------------
     * @return GroupEntity collection
     */
    
    public function findAll(array $conditions = [],array $options = [])
    {
        return $this->_GroupMapper->findAll($conditions,$options);
    }
    
    
     /**
     * -------------------------------------------------------------------------
     * Save Group
     * -------------------------------------------------------------------------
     * @object $GroupEntity
     * @return type
     */
    public function store(GroupInterface $GroupEntity)
    {
        return $this->_GroupMapper->save($GroupEntity);
    }
    
     /**
     * -------------------------------------------------------------------------
     * Delete Group
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function remove($id)
    {
        return $this->_GroupMapper->delete($id);
    }
 
}