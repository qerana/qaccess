<?php

/*
 * Copyright (C) 2020 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model\user;

use Qerapp\qaccess\model\user\UserService,
    Qerapp\qaccess\model\user\entity\UserInterface,
    Qerapp\qtoken\model\token\TokenService;

/**
 * *****************************************************************************
 * Description of UserTokenService
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class UserTokenService {

    protected
            $id_user,
            $token;

    public function __construct() {
        
    }

    /**
     *  Verify token service, for user activation
     */
    public function  verifyUserToken() {
        
        $TokenService = new TokenService();
        $TokenService->verify($this->token);

        $Token = $TokenService->getToken();
        $Token->getUser();

        new \Qerana\core\QSession();

        $_SESSION['S_activation_tokenid'] = $Token->id_token;
        $_SESSION['S_token_id_user'] = $Token->id_user;
        $_SESSION['S_token_user'] = $Token->User;

        // include form activation
        if ($Token->User instanceof UserInterface) {
            // first delete the token
           // $TokenService->delete($Token);
            return true;
        } else {
            \QException\Exceptions::showHttpStatus(404, 'Token dosnt have a valid user!!');
        }
    }

    /**
     *  ----------------------------------------------------------------------------
     * Activate user
     * ----------------------------------------------------------------------------
     */
    public function activateUser() {


        if (!isset($_SESSION['S_activation_tokenid']) AND ! isset($_SESSION['S_token_id_user'])) {

            \QException\Exceptions::showHttpStatus(404, 'trying to access activation user without id_token sessioned!!');
        } else {


            $UserService = new UserService;
            $UserService->activateUser($_SESSION['S_token_user']);
        }
    }

    /**
     *  Create user activation token
     */
    public function createUserToken(int $type) {

        $TokenService = new TokenService();
        $TokenService->createUserToken($this->id_user, $type);

        $Token = $TokenService->getToken();

        $TokenEmailService = new TokenEmailService($Token);
        $TokenEmailService->createTokenEmail();

        // @TODO:  send email to user
        $TokenEmailService->sendTokenEmail();
    }

    /**
     *  Process user reactivation token
     */
    public function createReactivationToken() {

        // first find email user
        $email_recovery = filter_input(INPUT_POST, 'f_user_email', FILTER_VALIDATE_EMAIL);

        $UserService = new UserService;
        $User = $UserService->findByEmail($email_recovery);

        if ($User instanceof UserInterface) {

            $this->id_user = $User->id_user;

            // desativate user
            $User->active = '0';
            $UserService->UserRepository->store($User);
            $this->createUserToken(2);
        } else {
            \QException\Exceptions::showError('Recovery.Error', 'Lo siento, ese mail no lo tenemos registrado!!');
        }
    }

    public function set_id_user(int $id_user) {
        $this->id_user = $id_user;
    }

    public function set_token(string $token) {
        $this->token = $token;
    }

}
