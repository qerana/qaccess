<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model\user\mapper;

use Ada\adapters\PDOAdapter,
    Ada\adapters\XmlAdapter,
    Ada\mapper\AdaDataMapper,
    Qerapp\qaccess\model\user\entity\UserEntity,
    Qerapp\qaccess\model\user\entity\UserInterface;

/*
  |*****************************************************************************
  | MAPPER CLASS for UserEntity
  |*****************************************************************************
  |
  | MAPPER User
  | @author TUPA,
  | @date 2019-08-22 19:26:36,
  |*****************************************************************************
 */

class UserXmlMapper extends AdaDataMapper implements UserMapperInterface
{


    public function __construct()
    {
        $user_xml = __DATA__.'xml/qaccess/users';
        parent::__construct(new XmlAdapter($user_xml, 'user', false));
    }

    /**
     * -------------------------------------------------------------------------
     * Get user by id
     * -------------------------------------------------------------------------
     * @param int $id
     * @return user entity
     */
    public function findById(int $id)
    {
        $row = $this->_Adapter->find(['id_user' => $id], ['fetch' => 'one']);

        // if row exists , the create a new entity, otherwise null is returned
        if (!$row) {
            return null;
        } else {
            return $this->createEntity($row);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Save entity
     * -------------------------------------------------------------------------
     */
    public function save(UserInterface $User)
    {

        $data = parent::getDataObject($User);


        if (is_null($User->id_user)) {
            $User->id_user = $this->_Adapter->insert($data);
        } else {
            $this->_Adapter->update($data, ['id_user' => $User->id_user]);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Delete entity
     * -------------------------------------------------------------------------
     * @param mixed $id
     * @return type
     */
    public function delete($id)
    {

        // if $id is a object and a UserEntity  
        if ($id instanceof UserInterface) {
            $id = $id->id;
        }

        return $this->_Adapter->delete(['id_user' => $id]);
    }

    /**
     * -------------------------------------------------------------------------
     * Create Entity
     * -------------------------------------------------------------------------
     * @param array $row
     * @return Entity
     */
    protected function createEntity(array $row): UserEntity
    {

        return new UserEntity($row);
    }

}
