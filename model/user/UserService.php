<?php

/*
 * Copyright (C) 2020 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model\user;

use Qerapp\qaccess\model\user\entity\UserEntity AS UserEntity,
    Qerapp\qaccess\model\user\mapper\UserMapper AS UserMapper,
    Qerapp\qaccess\model\user\mapper\UserMapperInterface,
    Qerapp\qaccess\model\user\repository\UserRepository AS UserRepository,
    Qerapp\qaccess\model\profile\mapper\ProfileMapper AS ProfileMapper,
    Qerapp\qaccess\model\profile\ProfileService,
    \Qerapp\qbasic\model\log\LogService,
    \Qerana\Configuration,
    \Qerapp\qaccess\model\UserCreatorService;

/*
  |*****************************************************************************
  | UserService
  |*****************************************************************************
  |
  | Service for Entity User
  | @author TUPA,
  | @date 2019-08-22 19:26:36,
  |*****************************************************************************
 */

class UserService {

    public
            $User,
            $UserRepository;
    protected
            $_Config,
            $_user_activation,
            // token o direct activation
            $_activation_mode = 'token',
            $_password,
            $_id_account_activation; // id_email_account

    public function __construct(UserMapperInterface $Mapper = null) {
        $MapperRepository = (is_null($Mapper)) ? new UserMapper : $Mapper;
        $this->UserRepository = new UserRepository($MapperRepository);
        $this->_Config = Configuration::singleton();
        $this->_user_activation = $this->_Config->get('_activation_user_');
    }

    /**
     *  Set the id_mail_account to send activation
     * @param int $id_account
     */
    public function set_account_email(int $id_account) {
        $this->_id_account_activation = $id_account;
    }

    /**
     * direct,token
     * @param type $mode
     */
    public function set_activation_mode($mode = null) {
        $this->_activation_mode = \helpers\Request::getValue('sw_activation_mode', $mode, FILTER_SANITIZE_STRING);
    }

    /**
     * get user logs
     * @param int $id_user
     */
    public function getLogsUser(int $id_user) {
        $LogService = new LogService();
        return $LogService->getUserLog($id_user);
    }

    /**
     * get user by profile
     * @param int $id_profile
     */
    public function getUsersByProfile(int $id_profile) {
        $UserRepository = new UserRepository(new UserMapper);
        return $UserRepository->findById_profile($id_profile, ['fetch' => 'all']);
    }

    /**
     * get user /add/edit dependencies
     * @return type
     */
    public function getDependencies() {

        $dependencies = [];

        // profiles
        $ProfileService = new ProfileService;
        $Profiles = $ProfileService->getAll();
        $dependencies['profiles'] = $Profiles;



        return $dependencies;
    }

    /**
     * -------------------------------------------------------------------------
     * Get all
     * -------------------------------------------------------------------------
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getAll($json = false) {

        $Collection = $this->UserRepository->findAll();
        return ($json === true) ? \helpers\Utils::parseToJson($Collection) : $Collection;
    }

    /**
     * -------------------------------------------------------------------------
     * Get by id
     * -------------------------------------------------------------------------
     * @param $id , id entity
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getById(int $id, $json = false) {

        $Entity = $this->UserRepository->findById_user($id);
        return ($json === true) ? \helpers\Utils::parseToJson($Entity) : $Entity;
    }

    /**
     * -------------------------------------------------------------------------
     * get user by username
     * -------------------------------------------------------------------------
     * @param string $username
     * @return type
     */
    public function getUserByUsername(string $username) {

        $User = $this->UserRepository->findByUsername($username, ['fetch' => 'one']);
        return ($User) ? $User : null;
    }

    /**
     * -------------------------------------------------------------------------
     * Check if username  is available to use
     * -------------------------------------------------------------------------
     * @param string $username
     */
    public function checkUsernameIsAvailable(string $username) {

        // find by username
        if (!$this->isEmailAvailable($username)) {
            \QException\Exceptions::showError('ErrorCreatingNewUser', 'Username :'
                    . $username . ' is not available to use, please pick another');
        }
    }

    /**
     *  Search in user , if email are already taked
     * @param string $email
     */
    public function isEmailAvailable(string $email) {

        if (!empty($this->UserRepository->findByUser_email($email))) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * find user by email 
     * @param string $email
     * @return type
     */
    public function findByEmail(string $email) {

        $UserRepository = new UserRepository(new UserMapper);
        return $UserRepository->findByUser_email($email, ['fetch' => 'one']);
    }

    /**
     * -------------------------------------------------------------------------
     * Check if email  is available to use
     * -------------------------------------------------------------------------
     * @param string $email
     */
    public function checkEmailIsAvailable(string $email) {

        // find by email
        if (!empty($this->findByEmail($email))) {
            \QException\Exceptions::showError('ErrorCreatingNewUser', 'Email : ' . $email . ' is already registered');
        }
    }


    /**
     * Save user
     * @param array $data
     */
    public function save(array $data = []) {

        $data_user = (empty($data)) ? \helpers\Request::getFormData() : $data;
        $this->User = new UserEntity($data_user);
        $this->UserRepository->store($this->User);
    }

    /**
     * Perform new user creation
     * @param string $mode , activation mode, direct;token
     * @param array $data
     */
    public function create(string $mode = '', array $data = []) {

        // activation mode
        $activation_mode = (empty($mode)) ? \helpers\Request::getValue('sw_activation_mode', $mode, FILTER_SANITIZE_STRING) : $mode;

        // data info, form array or from form
        $user_data = (empty($data)) ? \helpers\Request::getFormData() : $data;

        // create user objet with data fill
        $User = new UserEntity($user_data);

        $UserCreator = new \Qerapp\qaccess\model\UserCreatorService($User, $activation_mode);
        $result = $UserCreator->create();

        $this->User = $User;

        // if result is true
        if ($result) {

            // if activation mode = token
            if ($activation_mode == 'token') {

                $TokenEmailService = new TokenEmailService($UserCreator->Token);
                $TokenEmailService->id_account_sender = (!isset($this->_id_account_activation)) ? $this->_Config->get('_id_account_activation') : $this->_id_account_activation;
                $TokenEmailService->createTokenEmail();
                $TokenEmailService->sendTokenEmail();
            }

            return true;
        } else {
            return $UserCreator->getError();
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Create new user
     * -------------------------------------------------------------------------
     */
    public function createNewUser(array $data = []) {
        try {

            $user_data = (empty($data)) ? \helpers\Request::getFormData() : $data;


            $User = new UserEntity($user_data);
            $User->password = \helpers\Security::random(20);

            // lets check if username and email is available
            if (!isset($user_data['id_user'])) {
                $this->checkUsernameIsAvailable($user_data['username']);
                $this->checkEmailIsAvailable($user_data['user_email']);
                $User->active = ($this->_activation_mode == 'direct') ? '1' : '0';

                if (isset($user_data['password'])) {

                    $User->password = $user_data['password'];
                }
                $User->generatePassword();
            }

            // save new user
            $this->UserRepository->store($User);

            $this->User = $User;


            // create token activation only if token is activation method
            if ($this->_activation_mode === 'token') {
                $UserTokenService = new UserTokenService();
                $UserTokenService->set_id_user($User->id_user);

                $id_account_activation = (!isset($this->_id_account_activation)) ? $this->_Config->get('_id_account_activation') : $this->_id_account_activation;

                $UserTokenService->createUserToken(1, $id_account_activation);
            }
        } catch (\Exception $ex) {

            \QException\Exceptions::ShowException('NewUserCreate', $ex);
        }
    }

    /**
     *  Activate a user
     */
    public function activateUser(entity\UserInterface $User) {
        // clean all sessions
        $Session = new \Qerana\core\QSession();
        $Session->cleanSession();

        $User->password = filter_input(INPUT_POST, 'f_password', FILTER_SANITIZE_STRING);
        $User->generatePassword();
        $User->active = 1;

        return $this->UserRepository->store($User);
    }

    /**
     *  check current password
     */
    public function changePassword(int $id) {

        // get passwords form inputs
        $current_pass = \helpers\Request::get('s_current_password');
        $new_pass = \helpers\Request::get('f_password');
        $new_pass_verification = \helpers\Request::get('f_password_verification');

        // get user entity
        $User = $this->getById($id);

        // first check if current password is correct

        if (!password_verify($current_pass, $User->password)) {
            \QException\Exceptions::showError('Qaccess.Profile', 'Error de credenciales:password actual incorrecto');
        }

        // check password verification
        if ($new_pass !== $new_pass_verification) {
            \QException\Exceptions::showError('Qaccess.Profile', 'Error de credenciales:el nuevo password no coincide con la verificacion');
        }

        // actualizamos
        $User->password = $new_pass;
        $User->generatePassword();

        return $this->UserRepository->store($User);
    }

    /**
     * Delete a user
     * @param int $id
     * @return type
     * @ TODO, remove all user ACL
     */
   public function deleteUser(int $id) {

        // get user
        $User = $this->getById($id);
        if ($User instanceof UserEntity) {

            return $this->UserRepository->remove($id);
        } 
    }

}
