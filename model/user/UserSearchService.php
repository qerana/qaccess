<?php

/*
 * This file is part of QeranaProject
 * Copyright (C) 2020-2021  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model\user;

use Ada\AdaSearch,
    Qerapp\qaccess\model\user\mapper\UserMapper,
    Qerapp\qaccess\model\user\repository\UserRepository;

/**
 * Searcher for users
 *
 * @author diemarc
 */
class UserSearchService {

    protected
            $UserRepo;

    public function __construct() {

        $this->UserRepo = new UserRepository(new UserMapper());
    }

    /**
     * Perform global search in users
     * @param string $search
     */
    public function searchUsers(string $search) {

        $repos = [
            [
                'repo' => $this->UserRepo,
                'fields' => ['username', 'user_email', 'name', 'lastname','id_user'],
                'source' => 'Usuario',
            ]
            
        ];

        $AdaSearch = new AdaSearch($search, $repos);
        $AdaSearch->search();

        return $AdaSearch;
    }

}

