<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model\user\repository;

use Qerapp\qaccess\model\user\entity\UserInterface;

/*
  |*****************************************************************************
  | UserRepositoryInterface
  |*****************************************************************************
  |
  | Repository INTERFACE User
  | @author TUPA,
  | @date 2019-08-22 19:26:36,
  |*****************************************************************************
 */

interface UserRepositoryInterface
{

    public function findAll(array $conditions = [], array $options = []);

    public function findById_user(int $id_user, array $options = []);

    public function findById_profile(int $id_profile, array $options = []);

    public function findByUsername(string $username, array $options = []);

    public function findByPassword(string $password, array $options = []);

    public function findBySalt(string $salt, array $options = []);

    public function findByUser_email(string $user_email, array $options = []);

    public function findByName(string $name, array $options = []);

    public function findByLastname(string $lastname, array $options = []);

    public function findByActive(string $active, array $options = []);

    public function findByCreated_by(string $created_by, array $options = []);

    public function findByCreated_at(string $created_at, array $options = []);

    public function findByModify_by(string $modify_by, array $options = []);

    public function findByModify_at(string $modify_at, array $options = []);

    public function store(UserInterface $User);

    public function remove($id);
}
