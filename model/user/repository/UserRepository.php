<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model\user\repository;

use Qerapp\qaccess\model\user\mapper\UserMapperInterface,
    Qerapp\qaccess\model\user\entity\UserInterface;

/*
  |*****************************************************************************
  | UserRepository
  |*****************************************************************************
  |
  | Repository User
  | @author TUPA,
  | @date 2019-08-22 19:26:36,
  |*****************************************************************************
 */

class UserRepository implements UserRepositoryInterface
{

    private
            $_UserMapper;

    public function __construct(UserMapperInterface $Mapper)
    {

        $this->_UserMapper = $Mapper;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  id_user
     * ------------------------------------------------------------------------- 
     * @param id_user 
     */
    public function findById_user(int $id_user, array $options = [])
    {
        return $this->_UserMapper->findOne(["id_user" => $id_user], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  id_profile
     * ------------------------------------------------------------------------- 
     * @param id_profile 
     */

    public function findById_profile(int $id_profile, array $options = [])
    {
        return $this->_UserMapper->findAll(["id_profile" => $id_profile], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  username
     * ------------------------------------------------------------------------- 
     * @param username 
     */

    public function findByUsername(string $username, array $options = [])
    {
        return $this->_UserMapper->findOne(["username" => $username], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  password
     * ------------------------------------------------------------------------- 
     * @param password 
     */

    public function findByPassword(string $password, array $options = [])
    {
        return $this->_UserMapper->findAll(["password" => $password], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  salt
     * ------------------------------------------------------------------------- 
     * @param salt 
     */

    public function findBySalt(string $salt, array $options = [])
    {
        return $this->_UserMapper->findAll(["salt" => $salt], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  user_email
     * ------------------------------------------------------------------------- 
     * @param user_email 
     */

    public function findByUser_email(string $user_email, array $options = [])
    {
        return $this->_UserMapper->findOne(["user_email" => $user_email], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  name
     * ------------------------------------------------------------------------- 
     * @param name 
     */

    public function findByName(string $name, array $options = [])
    {
        return $this->_UserMapper->findAll(["name" => $name], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  lastname
     * ------------------------------------------------------------------------- 
     * @param lastname 
     */

    public function findByLastname(string $lastname, array $options = [])
    {
        return $this->_UserMapper->findAll(["lastname" => $lastname], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  active
     * ------------------------------------------------------------------------- 
     * @param active 
     */

    public function findByActive(string $active, array $options = [])
    {
        return $this->_UserMapper->findAll(["active" => $active], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  created_by
     * ------------------------------------------------------------------------- 
     * @param created_by 
     */

    public function findByCreated_by(string $created_by, array $options = [])
    {
        return $this->_UserMapper->findAll(["created_by" => $created_by], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  created_at
     * ------------------------------------------------------------------------- 
     * @param created_at 
     */

    public function findByCreated_at(string $created_at, array $options = [])
    {
        return $this->_UserMapper->findAll(["created_at" => $created_at], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  modify_by
     * ------------------------------------------------------------------------- 
     * @param modify_by 
     */

    public function findByModify_by(string $modify_by, array $options = [])
    {
        return $this->_UserMapper->findAll(["modify_by" => $modify_by], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  modify_at
     * ------------------------------------------------------------------------- 
     * @param modify_at 
     */

    public function findByModify_at(string $modify_at, array $options = [])
    {
        return $this->_UserMapper->findAll(["modify_at" => $modify_at], $options);
    }

    /**
     * -------------------------------------------------------------------------
     * Get all User
     * -------------------------------------------------------------------------
     * @return UserEntity collection
     */
    public function findAll(array $conditions = [], array $options = [])
    {
        return $this->_UserMapper->findAll($conditions, $options);
    }

    /**
     * -------------------------------------------------------------------------
     * Save User
     * -------------------------------------------------------------------------
     * @object $UserEntity
     * @return type
     */
    public function store(UserInterface $UserEntity)
    {
        return $this->_UserMapper->save($UserEntity);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete User
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function remove($id)
    {
        return $this->_UserMapper->delete($id);
    }
    
    

}
