<?php

/*
 * Copyright (C) 2020 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model\user;

use Qerapp\qtoken\model\token\interfaces\TokenInterface,
    Qerapp\qaccess\model\user\entity\UserInterface,
    Qerapp\qemail\model\email\interfaces\EmailInterface,
    Qerapp\qemail\model\email\entity\EmailEntity,
    Qerapp\qemail\model\email\EmailService,
    Qerapp\qemail\model\Qemail;

/**
 * *****************************************************************************
 *  Sends user token via email
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class TokenEmailService
{

    private
            $tpl_email_activation,
            $tpl_email_recovery;
    public
    /**
     * @var int 1,activation,2 recovery 
     */
            $email_type,
            // which account uses to send emails activations
            $id_account_sender = '1',
    /**
     *  @object token entity
     *  */
            $Token,
            $Email,
            $url_activation;

    public function __construct(TokenInterface $Token)
    {

        $this->Token = $Token;
        $this->Token->getUser();
        $coeficient = 7 * $this->Token->token_type;
        $this->url_activation = __URL__ . '/welcome/welcome/verification/' . $this->Token->token . $this->Token->User->id_user * $coeficient;

// set paths for templates
        $this->tpl_email_activation = realpath(__APPFOLDER__ . '/welcome/view/qaccess/tpl_email_activation.php');
        $this->tpl_email_recovery = realpath(__APPFOLDER__ . '/welcome/view/qaccess/tpl_email_recovery.php');

        // chekc if qemail is installed
        $ModuleService = new \Qerapp\qbasic\model\module\ModuleService();
        $ModuleService->module_name = 'qemail';
        $Qemail = $ModuleService->getModule();

        if (!$Qemail) {
            \QException\Exceptions::ShowSuccessful('UserTokenActivation Created!!!',
                    ' Cant sent email, beacause qemail hast not installed!!, copy this url and paste in your mail client<br> url:' . $this->url_activation);
        }
    }

    /**
     *   Fill activation email 
     */
    private function _fillActivationEmail()
    {

        $this->Email->subject = 'Activacion de cuenta ';

        $replaces = [
            '[{nombre_usuario}]' => $this->Token->User->full_name,
            '[{usuario}]' => $this->Token->User->username,
            '[{url}]' => ' <a href="' . $this->url_activation . '">aqui.</a>',
            '[{url_string}]' => $this->url_activation,
            '[{app}]' => __APPNAME__
        ];

        $this->Email->body = strtr(file_get_contents($this->tpl_email_activation), $replaces);
    }

    /**
     *  fill recovery email
     */
    private function _fillRecoveryEmail()
    {
        $this->Email->subject = 'Reactivacion de cuenta';

        $replaces = [
            '[{nombre_usuario}]' => $this->Token->User->full_name,
            '[{usuario}]' => $this->Token->User->username,
            '[{url}]' => ' <a href="' . $this->url_activation . '">aqui.</a>',
            '[{url_string}]' => $this->url_activation,
            '[{app}]' => __APPNAME__
        ];

        $this->Email->body = strtr(file_get_contents($this->tpl_email_recovery), $replaces);
    }

    /**
     *  Create email for user activation
     */
    public function createTokenEmail()
    {

        $this->Email = new EmailEntity();
        $this->Email->set_id_category($this->Token->token_type);
        $this->Email->type = $this->Token->token_type;
        $this->Email->destination = $this->Token->User->user_email;

        // activation email
        if ($this->Token->token_type == 1) {

            $this->_fillActivationEmail();
        }
        // recovery email
        else if ($this->Token->token_type == 2) {

            $this->_fillRecoveryEmail();
        }

        $EmailService = new EmailService();
        $EmailService->EmailRepository->store($this->Email);
    }
    
    
    /**
     *  Send token
     * @throws \RuntimeException
     */
    public function sendTokenEmail()
    {

        $EmailService = new EmailService();
        $Email = $EmailService->EmailRepository->findbyTypeAnEmail($this->Token->User->user_email, $this->Token->token_type);

        $Email->setAccount();
        if ($Email instanceof EmailEntity) {
            $Email->send();
        } else {
            throw new \RuntimeException('Email not found!!');
        }
    }

}
