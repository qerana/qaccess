<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model\user\entity;

/*
  |*****************************************************************************
  | INTERFACE  for user
  |*****************************************************************************
  |
  | Entity user
  | @author TUPA,
  | @date 2019-08-22 19:26:36,
  |*****************************************************************************
 */

interface UserInterface
{

    
    public function generatePassword();
    
//SETTERS

    public function set_id_user(int $id_user): void;

    public function set_id_profile(int $id_profile): void;

    public function set_username(string $username): void;

    public function set_password(string $password): void;
    
    public function set_direct_password($direct_password): void;

    public function set_salt(string $salt): void;

    public function set_user_email(string $user_email): void;

    public function set_name(string $name): void;

    public function set_lastname(string $lastname): void;

    public function set_active(string $active): void;

    public function set_created_by(string $created_by): void;

    public function set_created_at(string $created_at): void;

    public function set_modify_by(string $modify_by): void;

    public function set_modify_at(string $modify_at): void;
    
     public function encrypt_direct_password();

//GETTERS

    public function get_id_user();

    public function get_id_profile();

    public function get_username();

    public function get_password();

    public function get_salt();

    public function get_user_email();

    public function get_name();

    public function get_lastname();

    public function get_active();

    public function get_created_by();

    public function get_created_at();

    public function get_modify_by();

    public function get_modify_at();
    
    public function get_decrypt_pass();
}
