<?php

/*
 * Copyright (C) 2020 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model\user\entity;

use \Ada\EntityManager,
    Qerapp\qaccess\model\profile\entity\ProfileInterface;

/*
  |*****************************************************************************
  | ENTITY CLASS for User
  |*****************************************************************************
  |
  | Entity User
  | @author TUPA,
  | @date 2019-08-22 19:26:36,
  |*****************************************************************************
 */


class UserEntity extends EntityManager implements UserInterface,\JsonSerializable {

    //ATRIBUTES
    protected
    /** @var text(), $direct_password  */
            $_direct_password,
            /** @var int(11), $id_user  */
            $_id_user,
            /** @var int(11), $id_profile  */
            $_id_profile,
            /** @var varchar(20), $username  */
            $_username,
            /** @var varchar(255), $password  */
            $_password,
            /** @var varchar(32), $salt  */
            $_salt,
            /** @var varchar(255), $user_email  */
            $_user_email,
            /** @var varchar(150), $name  */
            $_name,
            /** @var varchar(150), $lastname  */
            $_lastname,
            /** @var tinyint(1), $active  */
            $_active,
            /** @var varchar(20), $created_by  */
            $_created_by,
            /** @var timestamp(), $created_at  */
            $_created_at,
            /** @var varchar(20), $modify_by  */
            $_modify_by,
            /** @var timestamp(), $modify_at  */
            $_modify_at;
    public
    /** @object profile object */
            $Profile,
            $full_name;

    public function __construct(array $data = []) {
        $this->populate($data);

        $this->Profile = \Ada\EntityRelation::hasOne($this, 'profile', 'id_profile');
    }

    /**
     * -------------------------------------------------------------------------
     * Generate password for new users
     * -------------------------------------------------------------------------
     */
    public function generatePassword() {

        $this->_password = password_hash($this->_password, PASSWORD_BCRYPT);
        $this->_salt = password_hash($this->_password, PASSWORD_BCRYPT);
    }

//SETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Setter for direct_password
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_direct_password($direct_password): void {
        $this->_direct_password = $direct_password;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_user
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_user($id_user): void {
        $this->_id_user = $id_user;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_profile
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_profile($id_profile): void {
        $this->_id_profile = $id_profile;
    }

    /**
     * -------------------------------------------------------------------------
     * Set ptofile info
     * -------------------------------------------------------------------------
     * @param type $Profile
     */
    public function set_profile($Profile) {

        if (!$Profile instanceof ProfileInterface) {
            throw new \InvalidArgumentException('Invalid profile');
        }

        $this->Profile = $Profile;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for username
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_username($username): void {
        $this->_username = $username;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for password
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_password($password): void {
        $this->_password = $password;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for salt
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_salt($salt): void {
        $this->_salt = $salt;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for user_email
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_user_email($user_email): void {
        $this->_user_email = $user_email;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for name
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_name($name): void {
        $this->_name = $name;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for lastname
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_lastname($lastname): void {
        $this->_lastname = $lastname;
        $this->full_name = $this->_name . ' ' . $this->_lastname;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for active
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_active($active): void {
        $this->_active = $active;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for created_by
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_created_by($created_by): void {
        $this->_created_by = $created_by;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for created_at
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_created_at($created_at): void {
        $this->_created_at = $created_at;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for modify_by
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_modify_by($modify_by): void {
        $this->_modify_by = $modify_by;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for modify_at
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_modify_at($modify_at): void {
        $this->_modify_at = $modify_at;
    }

    /**
     * Encrypt the direct_password
     */
    public function encrypt_direct_password() {
        $this->_direct_password = \helpers\Security::encrypt($this->_direct_password);
    }

//GETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Getter for direct_password
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_direct_password() {
        return filter_var($this->_direct_password, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_user
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_user() {
        return $this->_id_user;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_profile
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_profile() {
        return $this->_id_profile;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for username
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_username() {
        return $this->_username;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for password
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_password() {
        return $this->_password;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for salt
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_salt() {
        return $this->_salt;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for user_email
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_user_email() {
        return $this->_user_email;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for name
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_name() {
        return $this->_name;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for lastname
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_lastname() {
        return $this->_lastname;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for active
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_active() {
        return $this->_active;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for created_by
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_created_by() {
        return $this->_created_by;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for created_at
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_created_at() {
        return $this->_created_at;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for modify_by
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_modify_by() {
        return $this->_modify_by;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for modify_at
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_modify_at() {
        return $this->_modify_at;
    }

    public function get_active_status() {
        return ($this->_active === '1') ? 'Activo' : 'Inactivo';
    }

    public function get_fullname() {
        return $this->_name . ' ' . $this->_lastname;
    }
    
     /**
     * Get the decrypt password
     * @return type
     */
    public function get_decrypt_pass() {
        return \helpers\Security::decrypt($this->_direct_password);
    }

}
