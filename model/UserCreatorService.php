<?php

/*
 * Copyright (C) 2020 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model;

use Qerapp\qaccess\model\user\entity\UserEntity,
    \Qerapp\qaccess\model\user\mapper\UserMapper,
    \Qerapp\qaccess\model\user\repository\UserRepository,
    \Qerapp\qtoken\model\token\TokenService;

/**
 * User creation
 *
 * @author diemarc
 */
class UserCreatorService {

    private
            $errors = [],
            $UserRepo,
            $activation_mode;
    public
            $User,
            $Template,
            $Token,
            /**  the random password generated when activation mode is direct */
            $direct_password,
            $id_account_activation;

    public function __construct(UserEntity $User, string $activation_mode = 'token') {
        $this->User = $User;

        $this->setActivationMode($activation_mode);
        if ($this->checkUsernameAvailable() !== false) {
            $this->checkEmailAvailable();
        }
    }

    /**
     * Setter activation mode
     * @param string $activation_mode
     */
    public function setActivationMode(string $activation_mode) {
        $this->activation_mode = $activation_mode;

        // if mode = direct, the  create active status user
        if ($this->activation_mode == 'direct') {
            $this->User->active = '1';
            $this->User->password = \helpers\Security::random(8);
            $this->User->set_direct_password($this->User->password);
            $this->User->encrypt_direct_password();
        } 
        
        // mode as setted as token
        else if ($this->activation_mode == 'token') {
            $this->User->active = '0';
            // generate random password
            $this->User->password = \helpers\Security::random(20); // more large random passw
        } else {
            $this->errors[] = ['activation.mode' => 'Invalid activation mode ' . $this->activation_mode];
        }
    }

    /**
     * check if username is available
     * @return boolean
     */
    public function checkUsernameAvailable() {

        $UserRepo = new UserRepository(new UserMapper);
        $User = $UserRepo->findByUsername($this->User->username);

        if (!empty($User)) {
            $this->errors[] = ['username.used' => 'User already exists ' . $this->User->username];
            return false;
        }
    }

    /**
     * check if email is available
     * @return boolean
     */
    public function checkEmailAvailable() {

        $UserRepo = new UserRepository(new UserMapper);
        $User = $UserRepo->findByUser_email($this->User->user_email);

        if (!empty($User)) {
            $this->errors[] = ['email.used' => 'Email already exists ' . $this->User->user_email];
            return false;
        }
    }

    /**
     * Create new user
     * @return type
     */
    public function create() {

        // if are not errors
        if (empty($this->errors)) {

            // encrypt the password
            $this->User->generatePassword();


            // set time creation
            $this->User->created_at = date('Y-m-d H:i:s');
            $this->User->created_by = (isset($_SESSION['Q_id_user'])) ? $_SESSION['Q_id_user'] : 'qbot';


            // store object user
            $this->UserRepo = new UserRepository(new UserMapper);
            $this->UserRepo->store($this->User);

            if ($this->activation_mode == 'token') {
                $this->createActivationToken();
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     *  Create token activation
     */
    private function createActivationToken() {

        $TokenService = new TokenService;
        $TokenService->createUserToken($this->User->id_user, 1);

        $this->Token = $TokenService->getToken();
    }

    /**
     * Get errors
     * @return type
     */
    public function getError() {
        return $this->errors;
    }

}
