<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model;

use Qerapp\qbasic\model\module\ModuleService,
    Qerapp\qdevtools\model\creator\controller\ControllerXmlMapper AS ControllerMapper,
    Qerapp\qbasic\model\module\mapper\ModuleXmlMapper,
    Qerapp\qaccess\model\acl\AclService;

/**
 * *****************************************************************************
 * PermissionService
 * *****************************************************************************
 * Handle the user module permission
 * @author diemarc
 * *****************************************************************************
 */
class PermissionService
{

    public
            $modules_user = [],
            $controllers_user = [];

    public function __construct()
    {
     
        //
        echo $_SESSION['Q_username'];
        
        $AclService = new AclService();
        
        
        
        $AclCollection = $AclService->Repository->findAll(['user' => $_SESSION['Q_username']]);
        
        echo '<pre>';
        print_r($AclCollection);
        die();
        
        
        
        
        die();
    }

    /**
     * -------------------------------------------------------------------------
     * Get modules users
     * -------------------------------------------------------------------------
     */
    public function mapUserModules()
    {

        echo '<pre>';
        $ControllerMapper = new ControllerMapper();
        $controllers = $ControllerMapper->findAll();

        $modules = [];
        $controllers_array = [];

        //print_r($controllers);

        foreach ($controllers AS $k => $Controller):

            // convert each controller to array, to search
            $controllers_array[] = \helpers\Utils::convertObjectoArray($Controller, false);

            if ($Controller->Module->visible === '1') {
                $modules[$Controller->Module->id_module] = [
                    'module' => $Controller->Module->name,
                    'module_namespace' => $Controller->Module->module_namespace
                ];
            }
        endforeach;

        print_r($modules);


//        // extract the controllers
        foreach ($modules AS $key => $module_value):

            $controllers_keys = array_keys(array_column($controllers_array, '_module'), $module_value['module']);
            $controllers_module = [];
            foreach ($controllers_keys AS $k):
                if ($controllers_array[$k]['_visible'] === '1') {
                    $controllers_module[] = $controllers_array[$k]['_controller'];
                }

            endforeach;
            $modules[$key]['controllers'] = $controllers_module;

        endforeach;


        die();
        $_SESSION['Q_user_modules'] = $modules;
    }

}
