<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model;

use Qerapp\qaccess\model\user\UserService,
    Qerapp\qaccess\model\user\entity\UserInterface,
    Qerana\core\QSession,
    Qerapp\qbasic\model\log\LogService AS Logger,
    \Qerapp\qaccess\model\acl\AclprofileService;

/**
 * *****************************************************************************
 * Description of LoginService
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class LoginService
{

    protected
            $_user_login,
            $_pass_login,
            $_user_agent,
            $_user_address;
    private $_UserService,
            $_User,
            $_Session,
            $_user_landing;

    public function __construct(UserService $UserService)
    {
        $this->_UserService = $UserService;
        $this->_user_agent = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT', FILTER_SANITIZE_SPECIAL_CHARS);
        $this->_user_address = filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_VALIDATE_IP);
    }

    /**
     * -------------------------------------------------------------------------
     * Do login stuff
     * -------------------------------------------------------------------------
     */
    public function doLogin()
    {
        // check credentials
        $this->_Session = new QSession;
        $this->checkCredentials();

        // create login session
        $_SESSION['Q_id_user'] = $this->_User->id_user;
        $_SESSION['Q_username'] = $this->_User->username;
        $_SESSION['Q_fullname'] = $this->_User->get_fullname();
        $_SESSION['Q_login_string'] = hash('sha512', $this->_user_agent . $this->_User->id_user . $this->_User->user_email . $this->_User->salt);
        $_SESSION['Q_layout'] = $this->_User->Profile->layout; // wath template layout to load
        $_SESSION['Q_profile'] = $this->_User->Profile->profile_name; // profile object store in session
        $_SESSION['Q_id_profile'] = $this->_User->Profile->id_profile; // profile id
        $_SESSION['Q_levelacl'] = $this->_User->Profile->levelacl; // levelacl
        // register access

        $Logger = new Logger();
        $Logger->log_type = 'LoginSuccess';
        $Logger->sw_successfull = 1;
        $Logger->id_user = $this->_User->id_user;
        $Logger->message_log = 'Login successfull from ' . $this->_user_login;
        $Logger->registerLog();

        try {
            $this->_runProfileLogic();
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException('LogicProfile', $ex);
        }
        // run profile logic
        // $PermissionService->mapUserModules();
        // redirect
        $this->_user_landing = $this->_User->Profile->namespace_landing;
        \helpers\Redirect::to($this->_user_landing);
    }

    /**
     * Run profile logic
     */
    private function _runProfileLogic()
    {

        $this->_Session = new QSession;
        // map permissions -- comment this on date 29/10/2020 they dont make sense
        $AclProfileService = new AclprofileService;
        $AclProfileService->getModulesProfile();

        if ($this->_User->Profile->profile_name == 'dev') {
            $profile_namespace = 'Qerapp\\qaccess\\model\\profile\\profiles\\' . ucfirst($this->_User->Profile->profile_name) . 'Profile';
        } else {
            $profile_namespace = 'app\\welcome\\model\\profile\\' . ucfirst($this->_User->Profile->profile_name) . 'Profile';
        }


        try {
            // check if is callable the namespace
            if (is_callable(array($profile_namespace, 'buildProfile')) == false) {
                throw new \Exception('Profile logic ' . $profile_namespace . ' dont exists!!');
            } else {

                // create profile logic
                $Profile = new $profile_namespace;

                if (!$Profile instanceof profile\profiles\QProfileInterface) {
                    throw new \Exception('Profile logic dont implement qprofile ');
                }

                $Profile->buildProfile();
            }
        } catch (\Throwable $ex) {

            \QException\Exceptions::ShowException('CriticalError', $ex);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Check if session login is started
     * -------------------------------------------------------------------------
     */
    public function check()
    {


        $this->_Session = new QSession;
        if (isset($_SESSION['Q_id_user'], $_SESSION['Q_username'], $_SESSION['Q_login_string'])) {
            //return true;
            return true;
            //$User = $this->_UserService->getById($_SESSION['Q_id_user']);
            //            if ($User instanceof UserInterface) {
//
//                $login_string = hash('sha512', $this->_user_agent . $User->id_user . $User->user_email . $User->salt);
//
//                if ($login_string !== $_SESSION['Q_login_string']) {
//
//                    $this->goodBye('Invalid TokenSession!');
//                } else {
//                    $_SESSION['Q_id_user'] = $User->id_user;
//                    $_SESSION['Q_username'] = $User->username;
//                    $_SESSION['Q_login_string'] = $login_string;
//                }
//            } else {
//                $this->goodBye('Invalid Session!!');
//            }
        } else {


            $this->goodBye('Authentification required!!');
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Clean all session and show login page
     * -------------------------------------------------------------------------
     */
    public function goodBye(string $message = '')
    {
        $this->_Session = new QSession;
        //session_destroy();
        $this->_Session->cleanSession();
        $this->showLoginPage($message);
        die();
    }

    /**
     * -------------------------------------------------------------------------
     * Show login page
     * -------------------------------------------------------------------------
     */
    public function showLoginPage(string $message = '')
    {
        include realpath(__APPFOLDER__ . 'welcome/view/qaccess/login_page.php');
        die();
    }

    /**
     * -------------------------------------------------------------------------
     * Check credentials
     * -------------------------------------------------------------------------
     * @TODO , implement bad login service
     */
    public function checkCredentials()
    {
        $this->_Session = new QSession;
        $error = '';
        // first check if user exists

        $this->_User = $this->_UserService->getUserByUsername($this->_user_login);

        // if not exists
        if (is_null($this->_User)) {
            $error = ',comprueba tu nombre de usuario '; // user not exists
            $user_id = '0';
        }

        // user is inactive
        else if ($this->_User->active != 1) {
            $error = ',comprueba tu nombre de Usuario '; // exists but not active
            $user_id = $this->_User->id_user;
        }

        // password verification
        else if (!password_verify($this->_pass_login, $this->_User->password)) {

            $error = ',comprueba tu nombre de usuario y password '; // password mismatch fail
            $user_id = $this->_User->id_user;
        }

        if (!empty($error)) {

            $Logger = new Logger();
            $Logger->log_type = 'LoginFail';
            $Logger->sw_successfull = '0';
            $Logger->id_user = $user_id;
            $Logger->message_log = $error . $this->_user_login . ' ,pass =' . $this->_pass_login;
            $Logger->registerLog();
            \QException\Exceptions::showError('Error de acceso.', 'Credenciales incorrectas:' . $error);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Set user login
     * -------------------------------------------------------------------------
     * @param string $username
     */
    public function set_user_login($username): void
    {
        $this->_user_login = preg_replace('/[^a-zA-Z0-9_\-*.@]+/', '', filter_var($username, FILTER_SANITIZE_FULL_SPECIAL_CHARS));
    }

    /**
     * -------------------------------------------------------------------------
     * Set pass login
     * -------------------------------------------------------------------------
     * @param string $password
     */
    public function set_pass_login($password): void
    {
        $this->_pass_login = filter_var($password, FILTER_SANITIZE_SPECIAL_CHARS);
    }

    /**
     * -------------------------------------------------------------------------
     * Get user login
     * -------------------------------------------------------------------------
     * @return type
     */
    public function get_user_login()
    {
        return $this->_user_login;
    }

    /**
     * -------------------------------------------------------------------------
     * Get pass login
     * -------------------------------------------------------------------------
     * @return type
     */
    public function get_pass_login()
    {
        return $this->_pass_login;
    }

}
