<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model\profile\entity;

/*
  |*****************************************************************************
  | INTERFACE  for profile
  |*****************************************************************************
  |
  | Entity profile
  | @author TUPA,
  | @date 2019-08-22 20:52:11,
  |*****************************************************************************
 */

interface ProfileInterface
{

//SETTERS

    public function set_layout(string $layout): void;

    public function set_id_profile(int $id_profile): void;

    public function set_profile_name(string $profile_name): void;

    public function set_namespace_landing(string $namespace_landing): void;

//GETTERS

    public function get_layout();

    public function get_id_profile();

    public function get_profile_name();

    public function get_namespace_landing();
}
