<?php

/*
 * Copyright (C) 2020 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model\profile\entity;

use \Ada\EntityManager;

/*
  |*****************************************************************************
  | ENTITY CLASS for Profile
  |*****************************************************************************
  |
  | Entity Profile
  | @author TUPA,
  | @date 2019-08-22 20:52:11,
  |*****************************************************************************
 */

class ProfileEntity extends EntityManager implements ProfileInterface {

    //ATRIBUTES
    protected
    /** @var varchar(255), $layout  */
            $_layout,
            /** @var int(11), $id_profile  */
            $_id_profile,
            /** @var varchar(255), $profile_name  */
            $_profile_name,
            /** @var text(), $namespace_landing  */
            $_namespace_landing,
            /** @var int, the level acl to check */
            $_levelacl;

    public function __construct(array $data = []) {
        $this->populate($data);
    }

//SETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Setter for layout
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_layout($layout): void {
        $this->_layout = $layout;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_profile
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_profile($id_profile): void {
        $this->_id_profile = $id_profile;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for profile_name
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_profile_name($profile_name): void {
        $this->_profile_name = $profile_name;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for namespace_landing
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_namespace_landing($namespace_landing): void {
        $this->_namespace_landing = $namespace_landing;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for level_acl
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_levelacl($levelacl): void {
        $this->_levelacl = $levelacl;
    }

//GETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Getter for layout
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_layout() {
        return $this->_layout;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_profile
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_profile() {
        return $this->_id_profile;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for profile_name
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_profile_name() {
        return $this->_profile_name;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for namespace_landing
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_namespace_landing() {
        return $this->_namespace_landing;
    }

    /**
     * Get level acl
     * @return type
     */
    public function get_Levelacl() {
        return $this->_levelacl;
    }

}
