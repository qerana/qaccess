<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model\profile\mapper;
use Ada\adapters\PDOAdapter,
    Ada\mapper\AdaDataMapper,
    Qerapp\qaccess\model\profile\entity\ProfileEntity,
    Qerapp\qaccess\model\profile\entity\ProfileInterface;
/*
  |*****************************************************************************
  | MAPPER CLASS for ProfileEntity
  |*****************************************************************************
  |
  | MAPPER Profile
  | @author TUPA,
  | @date 2019-08-24 06:52:50,
  |*****************************************************************************
 */

class ProfileSqlMapper extends AdaDataMapper implements ProfileMapperInterface  {

    
     public function __construct()
    {
         
        parent::__construct(new PDOAdapter(\Ada\SqlLitePDO::singleton(),'qer_profile'));
        
    }
    
    
    /*
     * -------------------------------------------------------------------------
     * Find by id
     * -------------------------------------------------------------------------
     * @param int $id
     * @return EntityObject
     */
    public function findById(int $id)
    {
        $row = $this->_Adapter->find(['id_profile' => $id], ['fetch' => 'one']);

        // if row exists , the create a new entity, otherwise null is returned
        if (!$row) {
            return null;
        } else {
            return $this->createEntity($row);
        }
    }
    
    /**
     * -------------------------------------------------------------------------
     * Save entity
     * -------------------------------------------------------------------------
     */
    public function save(ProfileInterface $Profile){
        
        $data = parent::getDataObject($Profile);
        
        if (is_null($Profile->id_profile)) {
            $Profile->id_profile = $this->_Adapter->insert($data);
        } else {
           $this->_Adapter->update($data, ['id_profile' => $Profile->id_profile]);
        }
        
    }
    
     /**
     * -------------------------------------------------------------------------
     * Delete entity
     * -------------------------------------------------------------------------
     * @param mixed $id
     * @return type
     */
    public function delete($id)
    {

        // if $id is a object and a UserEntity  
        if ($id instanceof ProfileInterface) {
            $id = $id->id;
        }

        return $this->_Adapter->delete(['id_profile' => $id]);
    }
    
        /**
     * -------------------------------------------------------------------------
     * Create Entity
     * -------------------------------------------------------------------------
     * @param array $row
     * @return Entity
     */
    protected function createEntity(array $row): ProfileEntity
    {
        
        return  new ProfileEntity($row);
    }
 
 
}