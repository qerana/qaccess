<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model\profile\mapper;

use Qerapp\qaccess\model\profile\entity\ProfileInterface;

/*
  |*****************************************************************************
  | ProfileMapperInterface
  |*****************************************************************************
  |
  | MAPPER INTERFACE Profile
  | @author TUPA,
  | @date 2019-08-24 06:52:50,
  |*****************************************************************************
 */

interface ProfileMapperInterface
{

    public function findById(int $id);

    public function findAll(array $conditions = [], array $options = []);

    public function findOne(array $conditions);

    public function save(ProfileInterface $User);

    public function delete($id);
}
