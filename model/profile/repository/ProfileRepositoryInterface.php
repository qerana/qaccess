<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model\profile\repository;

use Qerapp\qaccess\model\profile\entity\ProfileInterface;

/*
  |*****************************************************************************
  | ProfileRepositoryInterface
  |*****************************************************************************
  |
  | Repository INTERFACE Profile
  | @author TUPA,
  | @date 2019-08-24 06:52:50,
  |*****************************************************************************
 */

interface ProfileRepositoryInterface
{

    public function findAll(array $conditions = [], array $options = []);

    public function findById_profile(int $id_profile, array $options = []);

    public function findByProfile_name(string $profile_name, array $options = []);

    public function findByNamespace_landing(string $namespace_landing, array $options = []);

    public function findByLayout(string $layout, array $options = []);

    public function store(ProfileInterface $User);

    public function remove($id);
}
