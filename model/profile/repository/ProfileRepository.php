<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model\profile\repository;

use Qerapp\qaccess\model\profile\mapper\ProfileMapperInterface,
    Qerapp\qaccess\model\profile\entity\ProfileInterface;

/*
  |*****************************************************************************
  | ProfileRepository
  |*****************************************************************************
  |
  | Repository Profile
  | @author TUPA,
  | @date 2019-08-24 06:52:50,
  |*****************************************************************************
 */

class ProfileRepository implements ProfileRepositoryInterface
{

    private
            $_ProfileMapper;

    public function __construct(ProfileMapperInterface $Mapper)
    {

        $this->_ProfileMapper = $Mapper;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  id_profile
     * ------------------------------------------------------------------------- 
     * @param id_profile 
     */
    public function findById_profile(int $id_profile, array $options = [])
    {
        return $this->_ProfileMapper->findOne(["id_profile" => $id_profile], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  profile_name
     * ------------------------------------------------------------------------- 
     * @param profile_name 
     */

    public function findByProfile_name(string $profile_name, array $options = [])
    {
        return $this->_ProfileMapper->findAll(["profile_name" => $profile_name], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  namespace_landing
     * ------------------------------------------------------------------------- 
     * @param namespace_landing 
     */

    public function findByNamespace_landing(string $namespace_landing, array $options = [])
    {
        return $this->_ProfileMapper->findAll(["namespace_landing" => $namespace_landing], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  layout
     * ------------------------------------------------------------------------- 
     * @param layout 
     */

    public function findByLayout(string $layout, array $options = [])
    {
        return $this->_ProfileMapper->findAll(["layout" => $layout], $options);
    }

    /**
     * -------------------------------------------------------------------------
     * Get all Profile
     * -------------------------------------------------------------------------
     * @return ProfileEntity collection
     */
    public function findAll(array $conditions = [], array $options = [])
    {
        return $this->_ProfileMapper->findAll($conditions, $options);
    }

    /**
     * -------------------------------------------------------------------------
     * Save Profile
     * -------------------------------------------------------------------------
     * @object $ProfileEntity
     * @return type
     */
    public function store(ProfileInterface $ProfileEntity)
    {
        return $this->_ProfileMapper->save($ProfileEntity);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete Profile
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function remove($id)
    {
        return $this->_ProfileMapper->delete($id);
    }

}
