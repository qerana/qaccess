<?php

/*
 * Copyright (C) 2020 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\model\profile;

use Qerapp\qaccess\model\profile\entity\ProfileEntity,
Qerapp\qaccess\model\profile\mapper\ProfileMapper AS ProfileMapper,
    Qerapp\qaccess\model\profile\mapper\ProfileMapperInterface,
    Qerapp\qaccess\model\profile\repository\ProfileRepository AS ProfileRepository;

/*
  |*****************************************************************************
  | ProfileService
  |*****************************************************************************
  |
  | Service for Entity Profile
  | @author TUPA,
  | @date 2019-08-24 06:52:50,
  |*****************************************************************************
 */

class ProfileService
{

    public
            $ProfileRepository;

    public function __construct(ProfileMapperInterface $Mapper = null)
    {
        $MapperRepository = (is_null($Mapper)) ? new ProfileMapper() : $Mapper;
        $this->ProfileRepository = new ProfileRepository($MapperRepository);
    }

    /**
     * -------------------------------------------------------------------------
     * Get all
     * -------------------------------------------------------------------------
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getAll($json = false)
    {

        $Collection = $this->ProfileRepository->findAll();
        return ($json === true) ? \helpers\Utils::parseToJson($Collection) : $Collection;
    }

    /**
     * -------------------------------------------------------------------------
     * Get by id
     * -------------------------------------------------------------------------
     * @param $id , id entity
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getById(int $id, $json = false)
    {

        $Entity = $this->ProfileRepository->findById_profile($id);
        return ($json === true) ? \helpers\Utils::parseToJson($Entity) : $Entity;
    }

    /**
     * -------------------------------------------------------------------------
     * Save 
     * -------------------------------------------------------------------------
     */
    public function save(array $data = [])
    {

        $data_to_save = (empty($data)) ? \helpers\Request::getFormData() : $data;

        $Profile = new ProfileEntity($data_to_save);
        $this->ProfileRepository->store($Profile);
    }
    
    

}
