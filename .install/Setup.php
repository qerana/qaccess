<?php

/*
 * This file is part of QAccess
 * Copyright (C) 2019-2020  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Setup {

    public function run() {

        // set qerana key
        \helpers\Security::generateQeranaKey();

        // cp structure
        $this->copyXmlStructure();

        // copy views
        $this->copyQaccessViews();

        // create user default
        $this->createUser();
    }

    /**
     * Create new user
     */
    private function createUser() {

        $control_digit = \helpers\Security::random(2);
        $username = trim(shell_exec('whoami'));
        $mail = $control_digit . '_' . $username . '@qerana.com';
        $password = \helpers\Security::random(8);

        $data = [
            'id_profile' => 1,
            'username' => $username,
            'password' => $password,
            'user_email' => $mail,
            'name' => $username,
            'lastname' => 'none'
        ];

        $User = new Qerapp\qaccess\model\user\UserService;
        $User->set_activation_mode('direct');
        $User->createNewUser($data);

        $info_user = '============================================================' . "\n";
        $info_user .= " QACCESS LOGIN INFORMATION \n";
        $info_user .= '============================================================' . "\n";
        $info_user .= '  User:' . $username . "\n";
        $info_user .= '  Password:' . $password . "\n";
        $info_user .= "\n";
        $info_user .= " * Stored in _data_/tmp/ \n";
        $info_user .= '___________________________________________________________' . "\n";

        echo $info_user;

        // lets create a file with this data for future uses
        $info_file = fopen(__DATA__ . 'tmp/info_login.txt', 'w');
        fwrite($info_file, $info_user);
        fclose($info_file);
    }

    /**
     *  Create xml structure to _DATA_/xml/qaccess
     */
    private function copyXmlStructure() {


        $dir_target = realpath(__DATA__) . '/xml/qaccess/';
        $dir_source = realpath(__QERAPPSFOLDER__ . 'qaccess/.install/_data/xml/');
        shell_exec('cp -r ' . $dir_source . ' ' . $dir_target);
    }

    /**
     *  copy login and qaccess view
     */
    private function copyQaccessViews() {

        $view_folder_source = realpath(__QERAPPSFOLDER__ . 'qaccess/view/');
        $controller_path_source = realpath(__QERAPPSFOLDER__ . 'qaccess/.install/_data/LoginController.php');

        $view_folder_target = realpath(__APPFOLDER__ . 'welcome/view');
        $controller_folder_target = realpath(__APPFOLDER__ . 'welcome/controller');
        // create qaccess view on welcome module
        mkdir($view_folder_target . '/qaccess', 0777, true);

        // copy files
        shell_exec('cp ' . $view_folder_source . '/login_page.php ' . $view_folder_target . '/qaccess/');
        shell_exec('cp ' . $view_folder_source . '/recovery.php ' . $view_folder_target . '/qaccess/');
        shell_exec('cp ' . $view_folder_source . '/user_activation.php ' . $view_folder_target . '/qaccess/');
        shell_exec('cp ' . $view_folder_source . '/user_reactivation.php ' . $view_folder_target . '/qaccess/');
        shell_exec('cp ' . $view_folder_source . '/user_profile.php ' . $view_folder_target . '/qaccess/');
        shell_exec('cp ' . $view_folder_source . '/user_password.php ' . $view_folder_target . '/qaccess/');
        shell_exec('cp ' . $view_folder_source . '/user_edit.php ' . $view_folder_target . '/qaccess/');
        shell_exec('cp ' . $view_folder_source . '/tpl_email_activation.php ' . $view_folder_target . '/qaccess/');
        shell_exec('cp ' . $view_folder_source . '/tpl_email_recovery.php ' . $view_folder_target . '/qaccess/');
        shell_exec('cp ' . $controller_path_source  . ' '.$controller_folder_target);
        
    }

}
