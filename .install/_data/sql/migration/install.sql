
CREATE DATABASE IF NOT EXISTS  `qerana_fw` /*!40100 DEFAULT CHARACTER SET utf8 */;
CREATE TABLE `qer_user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `id_profile` int(11) NOT NULL DEFAULT '1',
  `username` varchar(255) NOT NULL,
  `password` text,
  `salt` text,
  `user_email` varchar(255) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `lastname` varchar(140) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `modify_by` varchar(45) DEFAULT NULL,
  `modify_at` datetime DEFAULT NULL,
  `direct_password` text ,
  PRIMARY KEY (`id_user`),
  KEY `idx_username` (`username`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;



 CREATE TABLE IF NOT EXISTS  `qer_log` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `log_type` varchar(45) NOT NULL,
  `message_log` text,
  `log_timestamp` datetime NOT NULL,
  `sw_successfull` varchar(45) DEFAULT NULL,
  `user_agent_log` text,
  `address_ip` varchar(45) DEFAULT NULL,
  `instance` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*  password : invent*/
INSERT INTO `qer_user` (`id_user`,`id_profile`,`username`,`password`,`salt`,`user_email`,`name`,`lastname`,`active`,`created_by`,`created_at`,`modify_by`,`modify_at`) VALUES (1,1,'qfw','$2y$10$SvsfCwd0iJo.OzGwznk3sOthgaW2yC7G4epiwOYe.HMYM1ovoGrZy','$2y$10$Un0atR3AEjWgXFYx8JRaIOh4RE4Rls763/HttL5FLhv4sWMtqIEPO','qfw@qfw.com','qfw',NULL,1,NULL,NULL,NULL,NULL);



/** ACL level 1,2,3, action user */
CREATE TABLE `qer_profile_acl` (
  `id_profile_acl` int(11) NOT NULL AUTO_INCREMENT,
  `id_profile` int(11) NOT NULL,
  `module` varchar(100) NOT NULL,
  `controller` varchar(100) DEFAULT NULL,
  `action` varchar(100) DEFAULT NULL,
  `module_order` int(11) NOT NULL DEFAULT '0',
  `nav_content` text,
  PRIMARY KEY (`id_profile_acl`),
  KEY `idx_module` (`module`) USING BTREE,
  KEY `idx_controller` (`controller`),
  KEY `idx_action` (`action`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
