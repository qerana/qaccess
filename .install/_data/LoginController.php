<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\welcome\controller;

use Qerapp\qaccess\model\LoginService,
    Qerapp\qaccess\model\user\UserService,
    Qerapp\qaccess\model\user\mapper\UserMapper;

/**
 * *****************************************************************************
 * LoginController
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class LoginController {

    
    private 
            $UserService;
    
    
    public function __construct() {
        $this->UserService = new UserService(new UserMapper);
    
    }
    /**
     * -------------------------------------------------------------------------
     * Show login form
     * -------------------------------------------------------------------------
     */
    public function showLoginPage(): void {

        $Login = new LoginService($this->UserService);
        $Login->showLoginPage();
    }

    /**
     * -------------------------------------------------------------------------
     * Process login service
     * -------------------------------------------------------------------------
     */
    public function go() {

        $Login = new LoginService($this->UserService);
        $Login->set_user_login(\helpers\Request::get('f_username'));
        $Login->set_pass_login(\helpers\Request::get('f_password'));
        $Login->doLogin();
    }

    /**
     * -------------------------------------------------------------------------
     * Logout from qerana
     * -------------------------------------------------------------------------
     */
    public function logout() {
        $Login = new LoginService($this->UserService);
        $Login->goodBye();
    }

}
