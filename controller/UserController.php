<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\controller;

use Qerapp\qaccess\model\user\UserService;

defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');
/*
  |*****************************************************************************
  | CONTROLLER CLASS  User
  |*****************************************************************************
  |
  | Controller User
  | @author qDevTools,
  | @date 2020-02-08 07:01:33,
  |*****************************************************************************
 */

class UserController extends \Qerana\core\QeranaC {

    protected
            $_UserService;

    public function __construct() {
        parent::__construct();
        $this->_UserService = new UserService;
    }

    /**
     * -------------------------------------------------------------------------
     * Get all in json
     * -------------------------------------------------------------------------
     */
    public function getAllInJson() {
        return $this->_UserService->getAll(true);
    }

    /**
     * -------------------------------------------------------------------------
     * Get one in json
     * -------------------------------------------------------------------------
     */
    public function getOneInJson(int $id) {
        return $this->_UserService->getById($id, true);
    }

    /**
     * -------------------------------------------------------------------------
     * Show all
     * @return void
     * -------------------------------------------------------------------------
     */
    public function index(): void {



        $vars = [
            'Users' => $this->_UserService->getAll(),
//            'Plugins' => [
//                'data_json.js',
//                'app/user.js'
//            ]
        ];
        \Qerana\core\View::showView('user/index_user', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Add new 
     * @return void
     * -------------------------------------------------------------------------
     */
    public function add(): void {

        $vars = [
            'dependencies' => $this->_UserService->getDependencies()
        ];

        \Qerana\core\View::showForm('user/add_user', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Save new 
     * @return void
     * -------------------------------------------------------------------------
     */
     public function save(): void {
        $this->_UserService->create();
        \helpers\Redirect::toAction('detail/'.$this->_UserService->User->id_user);
    }


    /**
     * -------------------------------------------------------------------------
     * View
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function detail(int $id): void {

        $vars = [
            'id_user' => $id,
            'User' => $this->_UserService->getById($id),
            'Plugins' => [
                'data_json.js',
                'app/user.js'
            ]
        ];

        \Qerana\core\View::showView('user/detail_user', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Edit 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function edit(int $id): void {

        $vars = [
            'dependencies' => $this->_UserService->getDependencies(),
            'User' => $this->_UserService->getById($id),
            'Plugins' => [
                'data_json.js',
                'app/user.js'
            ]
        ];
        \Qerana\core\View::showForm('user/edit_user', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * modify
     * -------------------------------------------------------------------------
     * @return void
     */
    public function modify(): void {
        $this->_UserService->save();
        \helpers\Redirect::to('/qaccess/user/detail/' . $this->_UserService->User->id_user);
    }

    /**
     * -------------------------------------------------------------------------
     * modify user password
     * -------------------------------------------------------------------------
     * @return void
     */
    public function changemypassword(): void {
        $this->_UserService->changePassword($_SESSION['Q_id_user']);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function delete(int $id): void {
        $this->_UserService->deleteUser($id);
        \helpers\Redirect::toAction('index');
    }

}
