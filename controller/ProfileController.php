<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\controller;

use Qerapp\qaccess\model\profile\ProfileService;

defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');
/*
  |*****************************************************************************
  | CONTROLLER CLASS  Profile
  |*****************************************************************************
  |
  | Controller Profile
  | @author qdevtools,
  | @date 2020-02-10 06:31:29,
  |*****************************************************************************
 */

class ProfileController extends \Qerana\core\QeranaC
{

    protected
            $_ProfileService;

    public function __construct()
    {
        parent::__construct();
        $this->_ProfileService = new ProfileService();
    }

    /**
     * -------------------------------------------------------------------------
     * Show all
     * @return void
     * -------------------------------------------------------------------------
     */
    public function index(): void
    {

        $vars = [
            'Profiles' => $this->_ProfileService->getAll()
        ];
        \Qerana\core\View::showView('profile/index_profile', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Add new 
     * @return void
     * -------------------------------------------------------------------------
     */
    public function add(): void
    {

        $vars = [];

        \Qerana\core\View::showForm('profile/add_profile', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Save new 
     * @return void
     * -------------------------------------------------------------------------
     */
    public function save(): void
    {
        
    }

    /**
     * -------------------------------------------------------------------------
     * View
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function detail(int $id): void
    {

        $vars = [];

        \Qerana\core\View::showView('profile/detail_profile', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Edit 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function edit(int $id): void
    {

        $vars = [];
        \Qerana\View::showForm('profile/edit_profile', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * modify
     * -------------------------------------------------------------------------
     * @return void
     */
    public function modify(): void
    {
        
    }

    /**
     * -------------------------------------------------------------------------
     * Delete
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function delete(int $id): void
    {
        
    }

}
