<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qaccess\controller;

use Qerapp\qaccess\model\user\UserTokenService;


defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');
/*
  |*****************************************************************************
  | CONTROLLER CLASS  Account
  |*****************************************************************************
  |
  | Controller Account
  | @author qdevtools,
  | @date 2020-02-21 05:14:55,
  |*****************************************************************************
 */

class AccountController extends \Qerana\core\QeranaC {

    public function __construct() {
        parent::__construct();
    }

    /**
     *  Activate user account
     * @param string $token
     */
    public function verification(string $token = '') {

        if (empty($token)) {
            \QException\Exceptions::showHttpStatus(404, 'token verification without token!!');
        }

        $UserTokenService = new UserTokenService;
        $UserTokenService->set_token($token);
        if ($UserTokenService->verifyUserToken()) {
            \helpers\Redirect::to('/qaccess/account/useractivation');
        }
    }

    /**
     *  load user activation form
     */
    public function useractivation() {
        if (!isset($_SESSION['S_activation_tokenid']) AND!isset($_SESSION['S_token_id_user'])) {

            \QException\Exceptions::showHttpStatus(404, 'trying to access activation user without id_token sessioned!!');
        } else {
            include __APPFOLDER__ . '/welcome/view/qaccess/user_activation.php';
        }
    }

    /**
     *   Activate account
     */
    public function activate() {
        $UserTokenService = new UserTokenService();
        $UserTokenService->activateUser();
        \QException\Exceptions::ShowSuccessfulRedirect('Cuenta activada correctamente', 'Espere mientras preparamos tu pantalla de login', '/qaccess/login/showLoginPage');
    }

    /**
     *  show recovery form
     */
    public function recovery() {
        include __APPFOLDER__ . '/welcome/view/qaccess/recovery.php';
    }

    /**
     *  process recovery tasks
     */
    public function dorecovery() {

        $UserTokenService = new UserTokenService;
        $UserTokenService->createReActivationToken();
    }

   
}
